# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from exploreve.models.newsletter_entry import NewsletterEntry
from exploreve.utils.stats._per_day import get_stats_per_day


def get_newsletter_stats():
    total_entries = NewsletterEntry.select().count()
    days, mean_per_day, median_per_day = get_stats_per_day("newsletterentry")
    ret = {
        "count": total_entries,
        "days": {"per": days, "mean": mean_per_day, "median": median_per_day},
    }
    return ret
