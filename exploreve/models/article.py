# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import datetime
import random
import re
import string

import flask_login as login
from flask_admin.contrib.peewee import ModelView
from peewee import AutoField
from peewee import BooleanField
from peewee import CharField
from peewee import DateTimeField
from peewee import Model
from peewee import TextField

from exploreve import explodb


class Article(Model):
    """
    The main article model.
    """

    class Meta:
        database = explodb  # type: ignore

    # Article's ID (primary key)
    id = AutoField()
    # Article's url slug
    slug = CharField(unique=True)
    # Article's title
    title = CharField()
    # Article's content
    content = TextField()
    main_image = CharField()
    main_image_alt_text = CharField()

    # Article's creation date
    datetime_published = DateTimeField(default=datetime.datetime.utcnow)
    # Is the Article published
    is_published = BooleanField(index=True, default=False)

    def save(self, *args, **kwargs):
        if not self.slug:
            letters_digits = string.ascii_letters + string.digits
            rand = "".join(random.choice(letters_digits) for i in range(8))
            self.slug = re.sub(r"[^\w]+", "-", self.title.lower()) + "-" + rand
        ret = super(Article, self).save(*args, **kwargs)

        return ret

    def get_id(self) -> int:
        return int(self.id)

    def get_info(self) -> dict:
        article_info = {
            "id": self.id,
            "slug": self.slug,
            "title": self.title,
            "main_image": self.main_image,
            "main_image_alt_text": self.main_image_alt_text,
            "content": self.content,
            "datetime_published": self.datetime_published,
            "is_published": self.is_published,
        }
        return article_info


class ArticleAdmin(ModelView):
    model_class = Article

    def is_accessible(self):
        return login.current_user.is_authenticated


if not Article.table_exists():
    Article.create_table()
