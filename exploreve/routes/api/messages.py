from flask import Blueprint
from flask import jsonify
from flask import request
from peewee import DoesNotExist
from peewee import fn
from peewee import IntegrityError

from exploreve.errors import NotFoundError
from exploreve.models.message import Message
from exploreve.success.success import SuccessOutput
from exploreve.utils.constants import MOVEMENT_LIST
from exploreve.utils.decorators import auth_token_required
from exploreve.utils.decorators import validate_required_params

message_bp = Blueprint("messages", __name__)

REQUIRED_ARGS_MESSAGE_CREATION = {
    "display_name": str,
    "age": int,
    "movement": str,
    "job": str,
    "job_reason": str,
    "place": str,
    "place_reason": str,
    "personality": str,
    "personality_reason": str,
    "bored_activity": str,
    "scouting_activity": str,
    "like_about_friends": str,
}


@message_bp.route("/messages/random/<int:uid>", methods=["GET"])
@auth_token_required
def get_one_random_message(uid):
    """Get one random message
    @@@
    ### URL args
    | args | nullable | type | remark |
    |--------|--------|--------|--------|
    |    uid    |    false    |    int   |    The user's message id, so it is excluded    |
    ### JSON return
    ```json
    {
        "success": true
        "code": 200,
        "message": {
            "id": 1,
            "age": 22,
            "display_name": "Jules",
            "movement": "SGDF",
            "bored_activity": "Je regarde une série ou alors je fait du sport",
            "datetime_added": "Tue, 21 Apr 2020 17:47:01 GMT",
            "job": "Développeur",
            "job_reason": "J'adore découvrir de nouvelles choses et développer de nouvelles solutions, {...}",
            "like_about_friends": "Ils sont toujours avec moi lorsque j'en ai besoin <3",
            "personality": "Bruce Willis",
            "personality_reason": "Il ressemble a mon papa :3",
            "place": "La montagne",
            "place_reason": "J'adore le ski et la neige",
            "scouting_activity": "Encadrer des jeunes, et leur apprendre le meilleur comme d'autres l'ont {...}"
            "is_safe": true,
        },
    }
    ```
    @@@
    """
    try:
        own_message = Message.get_by_id(uid)
    except DoesNotExist:
        raise NotFoundError(f"Message id {uid} not found", "try again")

    try:
        returned_message = (
            Message.select()
            .where(
                Message.is_safe == True,  # noqa: E712
                Message.id != own_message.id,
                Message.movement != own_message.movement,
                (Message.age <= own_message.age + 3),
                (Message.age >= own_message.age - 3),
            )
            .order_by(fn.Random())
            .get()
        )
    except DoesNotExist:
        try:
            returned_message = (
                Message.select()
                .where(
                    Message.is_safe == True,  # noqa E712
                    Message.id != own_message.id,
                    Message.movement != own_message.movement,
                )
                .order_by(fn.Random())
                .get()
            )
        except DoesNotExist:
            returned_message = (
                Message.select()
                .where(
                    Message.is_safe == True, Message.id != own_message.id  # noqa E712
                )
                .order_by(fn.Random())
                .get()
            )
    returned_message.nb_times_read += 1
    returned_message.save()
    return SuccessOutput("message", returned_message.get_info())


@message_bp.route("/messages", methods=["POST"])
@auth_token_required
@validate_required_params(REQUIRED_ARGS_MESSAGE_CREATION)
def create_message():
    """Create a new message
    @@@
    ### JSON args
    | args | nullable | type | remark |
    |--------|--------|--------|--------|
    |    display_name    |    false    |    str   |    Display name of user who created souvenir    |
    |    age    |    false    |    int   |    Age of user    |
    |    movement    |    false    |    str   |    Movement of user.    |
    |    job    |    false    |    str   |    Job of user.    |
    |    job_reason    |    false    |    str   |    Reason for job of user.    |
    |    place    |    false    |    str   |    Place of user.    |
    |    place_reason    |    false    |    str   |    Reason for place of user.    |
    |    personality    |    false    |    str   |    Personality of user.    |
    |    personality_reason    |    false    |    str   |    Reason for personality of user.    |
    |    bored_activity    |    false    |    str   |    Bored activity of user.    |
    |    scouting_activity    |    false    |    str   |    Scouting activity of user.    |
    |    like_about_friends    |    false    |    str   |    What the user likes about friends of user.    |
    ### JSON success

    ```json
    {
        "success": true,
        "code": 200,
            "message": {
            "id": 1,
            "age": 22,
            "display_name": "Jules",
            "movement": "SGDF",
            "bored_activity": "Je regarde une série ou alors je fait du sport",
            "datetime_added": "Tue, 21 Apr 2020 17:47:01 GMT",
            "job": "Développeur",
            "job_reason": "J'adore découvrir de nouvelles choses et développer de nouvelles solutions, {...}",
            "like_about_friends": "Ils sont toujours avec moi lorsque j'en ai besoin <3",
            "personality": "Bruce Willis",
            "personality_reason": "Il ressemble a mon papa :3",
            "place": "La montagne",
            "place_reason": "J'adore le ski et la neige",
            "scouting_activity": "Encadrer des jeunes, et leur apprendre le meilleur comme d'autres l'ont {...}"
            "is_safe": true,
        },
    }
    ```
    @@@
    """
    data = request.get_json()
    display_name = data["display_name"]
    age = data["age"]
    movement = data["movement"]
    job = data["job"]
    job_reason = data["job_reason"]
    place = data["place"]
    place_reason = data["place_reason"]
    personality = data["personality"]
    personality_reason = data["personality_reason"]
    bored_activity = data["bored_activity"]
    scouting_activity = data["scouting_activity"]
    like_about_friends = data["like_about_friends"]

    error_list = []

    # Check 0 < age > 120
    if age <= 0 or age > 120:
        error_list.append(
            {
                "field": "age",
                "message": "Incorrect age {}. Must be between 0 and 120".format(age),
            }
        )
    # Check movement list
    if movement not in MOVEMENT_LIST:
        error_list.append(
            {
                "field": "movement",
                "message": "Incorrect movement {}. Must be one of {}".format(
                    movement, MOVEMENT_LIST
                ),
            }
        )

    if error_list:
        return (
            jsonify({"success": False, "status_code": 400, "errors": error_list}),
            400,
        )

    try:
        new_message = Message(
            display_name=display_name,
            age=age,
            movement=movement,
            job=job,
            job_reason=job_reason,
            place=place,
            place_reason=place_reason,
            personality=personality,
            personality_reason=personality_reason,
            bored_activity=bored_activity,
            scouting_activity=scouting_activity,
            like_about_friends=like_about_friends,
        )
        new_message.save()
    except IntegrityError as e:
        if "message_display_name" in str(e):
            error_list.append(
                {
                    "field": "display_name",
                    "message": "Display name {} is already used".format(display_name),
                }
            )
        else:
            raise e
    if error_list:
        return (
            jsonify({"success": False, "status_code": 400, "errors": error_list}),
            400,
        )
    else:
        return SuccessOutput("message", new_message.get_info())
