# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint
from flask import request
from peewee import IntegrityError
from requests import post
from sentry_sdk import capture_exception
from sentry_sdk import capture_message

from exploreve import MAILING_LIST
from exploreve.models.newsletter_entry import NewsletterEntry
from exploreve.success import Success
from exploreve.utils.decorators import auth_token_required
from exploreve.utils.decorators import validate_required_params

newsletter_bp = Blueprint("newsletter", __name__)


REQUIRED_KEYS_EMAIL_SUB = {"email": str}

# The url for the mailtrain instance
url = (
    "https://"  # type: ignore
    + MAILING_LIST["HOST"]  # type: ignore
    + "/api/subscribe/"  # type: ignore
    + MAILING_LIST["ID"]  # type: ignore
)  # type: ignore
get_data = {"access_token": MAILING_LIST["TOKEN"]}  # type: ignore


@newsletter_bp.route("/newsletter/subscribe", methods=["POST"])
@auth_token_required
@validate_required_params(REQUIRED_KEYS_EMAIL_SUB)
def subscribe_to_newsletter():
    """Register an email to the newsletter
    @@@
    #### JSON args
    | args | nullable | type | remark |
    |--------|--------|--------|--------|
    |    email    |    false    |    str   |    Email to add to newsletter    |

    #### return JSON
    ```json
    {
        "success": true,
        "message": "Successfully added entry to DB",
        "code": 200,
    }
    ```
    ### Notes:
      If the email is already in the table, it will still return a 200\
      as a success and ignore the fact that the email is already in the database

      If there is a misspell on the email then a message will be sent to sentry to be fixed manually
    @@@
    """
    data = request.get_json()
    data["email"] = data["email"].lower()

    try:
        NewsletterEntry(email=data["email"]).save()

        post_data = {"EMAIL": data["email"]}
        r = post(url, params=get_data, data=post_data)
        data = r.json()
        if "error" in data:
            capture_message(
                f'EMAIL {post_data["EMAIL"]} was not accepted by mailtrain : {data}'
            )
    except IntegrityError:
        # If the mail already exists in DB, ignore the error and return a success none the less
        pass
    except KeyError as e:
        # If mailtrain does not answer properly
        capture_exception(e)
    except ValueError as e:
        # If mailtrain does not answer properly
        capture_exception(e)
    return Success("Successfully added entry to DB")
