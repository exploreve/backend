# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import datetime

import flask_login as login
from flask_admin.contrib.peewee import ModelView
from peewee import AutoField
from peewee import BooleanField
from peewee import CharField
from peewee import Model
from peewee import TimestampField

from exploreve import explodb


class User(Model):
    """
    The main user model.
    """

    class Meta:
        database = explodb  # type: ignore

    # User's ID (primary key)
    id = AutoField()
    # the user's first name
    first_name = CharField()
    # The user's last name
    last_name = CharField()
    # The user's email address
    email = CharField()
    # The user's password, hashed with 'sha256'
    password = CharField()
    # The user's creation date
    creation_date = TimestampField(default=datetime.datetime.utcnow().timestamp())
    # When the user was last modified (by him or an admin)
    last_modified = TimestampField(default=datetime.datetime.utcnow().timestamp())
    # True if user has its email confirmed. False otherwise
    is_confirmed = BooleanField(default=False)
    # Date when the user was confirmed
    confirmed_on = TimestampField(null=True)
    # Either "admin" or "server".
    confirmed_by = CharField(null=True)

    def get_id(self) -> int:
        return int(self.id)

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    # Required for administrative interface
    def __unicode__(self):
        return self.username

    def get_info(self) -> dict:
        user_dict = {
            "first_name": self.first_name,
            "last_name": self.last_name,
            "email": self.email,
            "id": self.id,
            "creation_date": self.creation_date,
            "last_modified": self.last_modified,
            "is_confirmed": self.is_confirmed,
            "confirmed_on": self.confirmed_on,
            "confirmed_by": self.confirmed_by,
        }
        return user_dict


class UserAdmin(ModelView):
    model_class = User

    def is_accessible(self):
        return login.current_user.is_authenticated


def user_create(
    first_name: str, last_name: str, email: str, hashed_password: str
) -> User:
    new_user = User(
        first_name=first_name,
        last_name=last_name,
        email=email.lower(),
        password=hashed_password,
    )
    new_user.save()
    return new_user


if not User.table_exists():
    User.create_table()
