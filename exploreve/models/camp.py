# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import datetime
from typing import Any
from typing import Dict
from typing import List
from typing import Tuple

import flask_login as login
from botocore.exceptions import ClientError
from flask import url_for
from flask_admin.contrib.peewee import ModelView
from peewee import AutoField
from peewee import BooleanField
from peewee import CharField
from peewee import DateTimeField
from peewee import IntegerField
from peewee import Model

from exploreve import cellar_bucket
from exploreve import cellar_bucket_name
from exploreve import explodb
from exploreve.errors import NotFoundError
from exploreve.utils.cellar import create_presigned_post
from exploreve.utils.cellar import create_presigned_url
from exploreve.utils.cellar import PresignedDict


class CampTile(Model):
    """One tile of the camp"""

    id = AutoField()
    x = IntegerField()
    y = IntegerField()
    is_safe = BooleanField(default=True)
    is_reviewed = BooleanField(default=False)
    is_submitted = BooleanField(default=False)
    extension = CharField(default="png")
    datetime_added = DateTimeField(default=datetime.datetime.utcnow)

    class Meta:
        database = explodb  # type: ignore
        indexes = ((("x", "y"), True),)  # unique

    @property
    def filepath(self):
        return f"camp/{self.x}/{self.y}.{self.extension}"

    @classmethod
    def create_next(cls):
        """WARNING: moderated tiles should either not be replaced or be replaced keeping the id"""
        last_tile = cls.select().order_by(-CampTile.id).get()
        x, y = last_tile.get_next_coords()
        t = CampTile(x=x, y=y)
        t.save()
        return t

    def __str__(self):
        return (
            f"CampTile {self.id} at {self.x},{self.y} added at {self.datetime_added}"
            f"Sa{self.is_safe}R{self.is_safe}Su{self.is_submitted}"
        )

    def move_to(self, x: int, y: int):
        """Moves tile to the coordinates x,y

        This also moves the file in cellar if it exists
        """
        if x == self.x and y == self.y:
            return

        old_filepath = self.filepath

        if CampTile.get_or_none(x=x, y=y) is not None:
            raise ValueError("Previous tile does exist at ", (x, y))

        self.x, self.y = x, y
        new_filepath = self.filepath

        try:
            copy_source = {"Bucket": cellar_bucket_name, "Key": old_filepath}
            cellar_bucket.copy(copy_source, new_filepath)
        except ClientError:
            pass
        else:
            cellar_bucket.delete_objects(Delete={"Objects": [{"Key": old_filepath}]})
        finally:
            self.save()

    def get_url(self) -> str:
        """Gets the url to see/download the file"""
        url = create_presigned_url(self.filepath)
        if url is None:
            raise NotFoundError("Tile not found in cellar", "Add it to the cellar")
        return url

    def get_info(self) -> Dict[str, Any]:
        """Returns the info about the tile in a dict"""
        return {
            "id": self.id,
            "x": self.x,
            "y": self.y,
            "extension": self.extension,
            "filepath": self.filepath,
            "url": url_for("camps.get_camptile", x=self.x, y=self.y),
            "is_safe": self.is_safe,
            "is_reviewed": self.is_reviewed,
            "is_submitted": self.is_submitted,
            "datetime_added": self.datetime_added,
        }

    def get_form(self) -> PresignedDict:
        """Generate a presigned URL Cellar POST request to upload a camp tile

        :return: Tuple with the following keys:
            - URL to post to
            - Dictionary of form fields and values to submit with the POST
        """
        r = create_presigned_post(self.filepath)
        if r is not None:
            self.is_submitted = True
            self.save()
            return r
        else:
            raise NotFoundError("Form not found", "Dunno what happened")

    def get_nearby_tiles(self) -> List["CampTile"]:
        """Returns all nearby tiles"""
        x = self.x
        y = self.y
        query = CampTile.select().where(
            (CampTile.x >= x - 1)
            & (CampTile.x <= x + 1)
            & (CampTile.y >= y - 1)
            & (CampTile.y <= y + 1)
            & (CampTile.id != self.id)
        )
        tiles = [tile for tile in query]
        return tiles

    def get_next_coords(self) -> Tuple[int, int]:
        """Returns the next x and y coordinates"""
        x = self.x
        y = self.y
        if x == y:
            if x > 0:
                return x, y - 1
            else:
                return x, y + 1
        elif x == -y and y < 0:
            return x - 1, y
        elif 1 - x == y and y > 0:
            return x + 1, y
        elif x > 0 and y < x > -y:
            return x, y - 1
        elif x < 0 and x < y < -x + 1:
            return x, y + 1
        elif y > 1 and 1 - y < x < y:
            return x + 1, y
        else:  # y < 0 and y < x < -y
            return x - 1, y

    def get_previous_coords(self) -> Tuple[int, int]:
        """Returns the previous x and y coordinates"""
        x = self.x
        y = self.y
        if x == y:
            if x > 0:
                return x - 1, y
            elif x == 0:
                return 0, 0  # previous is itself
            else:
                return x + 1, y
        elif x == -y and y < 0:
            return x, y + 1
        elif 1 - x == y and y > 0:
            return x, y - 1
        elif x > 0 and y < x > -y:
            return x, y + 1
        elif x < 0 and x < y < -x + 1:
            return x, y - 1
        elif y > 1 and 1 - y < x < y:
            return x - 1, y
        else:  # y < 0 and y < x < -y
            return x + 1, y


class CampTileAdmin(ModelView):
    model_class = CampTile

    def is_accessible(self):
        return login.current_user.is_authenticated


if not CampTile.table_exists():
    CampTile.create_table()
