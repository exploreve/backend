# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from collections import Counter
from math import floor
from statistics import mean
from statistics import median
from typing import Any
from typing import Dict

from exploreve.models.message import Message
from exploreve.utils.stats._per_day import get_stats_per_day


def _get_messages_age_stats() -> Dict[str, Any]:
    message_ages = [s.age for s in Message.select(Message.age)]
    c = Counter(message_ages)
    top_3_ages = c.most_common(3)
    num_diff_ages = len(c)
    num_ages = sum(c.values())
    mean_age = floor(mean(message_ages))
    median_age = floor(median(message_ages))
    ret = {
        "top_3": top_3_ages,
        "num_different": num_diff_ages,
        "total": num_ages,
        "mean": mean_age,
        "median": median_age,
    }
    return ret


def _get_messages_movement_stats() -> Dict[str, Any]:
    souvenir_movement = (s.movement for s in Message.select(Message.movement))
    c = Counter(souvenir_movement)
    top_3_movements = c.most_common(3)
    ret = {"top_3": top_3_movements}
    return ret


def _get_nb_times_read_stats() -> Dict[str, Any]:
    nb_times_read = [s.nb_times_read for s in Message.select(Message.nb_times_read)]
    return {
        "total": sum(nb_times_read),
        "mean": floor(mean(nb_times_read)),
        "median": floor(median(nb_times_read)),
    }


def get_messages_stats() -> Dict[str, Any]:
    # TODO: Fuzzy search on places, personalities and jobs for stats on them
    total_entries = Message.select().count()
    days, mean_per_day, median_per_day = get_stats_per_day("message")
    ret = {
        "age": _get_messages_age_stats(),
        "days": {"per": days, "mean": mean_per_day, "median": median_per_day},
        "movement": _get_messages_movement_stats(),
        "times_read": _get_nb_times_read_stats(),
        "count": total_entries,
    }
    return ret
