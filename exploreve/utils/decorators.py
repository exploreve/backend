# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import os
from functools import wraps
from typing import Optional

from flask import request
from werkzeug.exceptions import BadRequest

from exploreve.errors import BadRequestError
from exploreve.errors import UnauthorizedError


def validate_required_params(required: dict, optional: Optional[dict] = None):
    if optional is None:
        optional = {}

    def decorator(fn):
        """Decorator that checks for the required parameters"""

        @wraps(fn)
        def wrapper(*args, **kwargs):
            # If the json body is missing something (`,` for example), throw an error
            try:
                data = request.get_json()
            except BadRequest:
                raise BadRequestError(
                    "The Json Body is malformed", "Please check it and try again"
                )

            # If the data dict is empty
            if not data:
                raise BadRequestError(
                    "Missing json body.", "Please fill your json body"
                )

            missing = []
            for item in required.keys():
                # If a key is missing in the sent data
                if item not in data.keys():
                    missing.append(item)
            if missing:
                raise BadRequestError(
                    "Missing keys {}.".format(missing),
                    "Complete your json body and try again",
                )

            for item in data.keys():
                # If there's an unwanted key in the sent data
                if item not in required.keys() and item not in optional.keys():
                    raise BadRequestError(
                        "You can't specify key '{}'.".format(item),
                        "You are only allowed to specify the fields {}"
                        ".".format(required.keys()),
                    )

            for key, value in data.items():
                if not value:
                    if required[key] == int:
                        pass
                    else:
                        raise BadRequestError(
                            f"The item {key} cannot be None or empty",
                            "Please try again.",
                        )

            wrong_types = [
                r for r in required.keys() if not isinstance(data[r], required[r])
            ]
            wrong_types += [
                r
                for r in optional.keys()
                if r in data and not isinstance(data[r], optional[r])
            ]

            if wrong_types:
                raise BadRequestError(
                    "{} is/are the wrong type.".format(wrong_types),
                    "It/They must be respectively {} and {}".format(required, optional),
                )

            return fn(*args, **kwargs)

        return wrapper

    return decorator


def auth_token_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        token = request.headers.get("exploreve-api-auth-token", None)
        if token:
            if token != os.getenv("EXPLOREVE_API_AUTH_TOKEN"):
                raise UnauthorizedError("Incorrect Auth Token", "Try again")
        else:
            raise UnauthorizedError("Missing Auth Token", "Try again")
        return f(*args, **kwargs)

    return decorated_function
