# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import random

from colour import Color

from exploreve.errors import BadRequestError


def validate_color(color: str) -> str:
    """
    :param color: a hex color string
    :return: Returns the hex formatted string : #ffffff
    """
    if len(color) == 7:
        color = color[1:]

    if len(color) == 6:
        try:
            int("0x" + color, 16)
        except ValueError:
            BadRequestError(
                f"Color {color} is not acceptable",
                "The only formats available are '#abc123' and 'abc123'",
            )
    else:
        raise BadRequestError(
            f"Color {color} is not acceptable",
            "The only formats available are '#123abc' and '123abc'",
        )

    return "#" + color


def get_4_random_colors():
    main_color = "#" + "".join([random.choice("0123456789ABCDEF") for j in range(6)])
    c = Color(main_color)
    c.set_luminance(0.8)
    lighter_color = c.get_hex() + "78"
    c.set_saturation(0.6)
    c.set_luminance(0.6)
    darker_color = c.get_hex()
    c.set_luminance(0.7)
    other_colorstyle = c.get_hex()
    return main_color, lighter_color, darker_color, other_colorstyle


def get_random_color_sets(amount: int):
    color_set = []
    for x in range(0, amount):
        (
            main_color,
            lighter_color,
            darker_color,
            other_colorstyle,
        ) = get_4_random_colors()
        color_set.append(
            {
                "main_color": main_color,
                "lighter_color": lighter_color,
                "darker_color": darker_color,
                "other_colorstyle": other_colorstyle,
            }
        )
    return color_set
