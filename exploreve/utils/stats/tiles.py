# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from exploreve.models.camp import CampTile
from exploreve.utils.stats._per_day import get_stats_per_day


def get_tiles_stats():
    total_entries = CampTile.select().count()
    unsafe_tile_count = (
        CampTile.select().where(CampTile.is_safe == False).count()  # noqa: E712
    )
    unmoderated_tile_count = (
        CampTile.select().where(CampTile.is_reviewed == False).count()  # noqa: E712
    )
    days, mean_per_day, median_per_day = get_stats_per_day("camptile")
    ret = {
        "count": total_entries,
        "days": {"per": days, "mean": mean_per_day, "median": median_per_day},
        "unsafe_count": unsafe_tile_count,
        "safe_count": total_entries - unsafe_tile_count,
        "unmoderated_count": unmoderated_tile_count,
        "moderated_count": total_entries - unmoderated_tile_count,
    }
    return ret
