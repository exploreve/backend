# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from flask_pagedown.fields import PageDownField
from flask_wtf import Form
from wtforms.fields import StringField
from wtforms.fields import SubmitField
from wtforms.validators import input_required


class ArticleForm(Form):
    title = StringField(
        "Title", validators=[input_required()], render_kw={"placeholder": "Titre"}
    )
    content = PageDownField(
        "Enter your markdown",
        validators=[input_required()],
        render_kw={"placeholder": "Content"},
    )
    main_image = StringField(
        "Main image url",
        validators=[input_required()],
        render_kw={"placeholder": "Main Image url"},
    )
    main_image_alt_text = StringField(
        validators=[input_required()], render_kw={"placeholder": "Main image alt text"}
    )
    submit = SubmitField("Submit")
