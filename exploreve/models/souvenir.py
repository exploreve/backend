# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import datetime
from typing import Any
from typing import Dict

import flask_login as login
from flask_admin.contrib.peewee import ModelView
from Geohash import decode as gh_decode
from peewee import AutoField
from peewee import BooleanField
from peewee import CharField
from peewee import DateTimeField
from peewee import IntegerField
from peewee import Model
from peewee import TextField
from playhouse.postgres_ext import JSONField

from exploreve import explodb


class Souvenir(Model):
    """A souvenir"""

    class Meta:
        database = explodb  # type: ignore

    id = AutoField()
    datetime_added = DateTimeField(default=datetime.datetime.utcnow)
    # Where the souvenir is on the map
    geohash = CharField(null=False)
    # Id of the scarf for the pin on the map
    scarf_id = IntegerField()

    # Displayed name of the person who wrote the souvenir
    display_name = CharField(null=False)
    age = IntegerField(null=False)
    movement = CharField(
        null=False,
        choices=[
            ("SGDF", "SGDF"),
            ("EEDF", "EEDF"),
            ("EEUDF", "EEUDF"),
            ("EEIF", "EEIF"),
            ("SMF", "SMF"),
            ("EDLN", "EDLN"),
            ("AGSE", "AGSE"),
            ("SUF", "SUF"),
            ("FEE", "FEE"),
            ("Ecl Neutres", "Ecl Neutres"),
            ("autre", "autre"),
        ],
    )

    souvenir_type = CharField(
        null=False, choices=[("url", "url"), ("text", "text"), ("none", "none")]
    )
    souvenir_content = TextField()
    # 3 word list
    word_list = JSONField(null=False)

    # Just in case we need to moderate
    is_safe = BooleanField(default=True)

    def get_coords(self):
        lat, lng = gh_decode(self.geohash)
        return [lat, lng]

    def get_info(self) -> Dict[str, Any]:
        return {
            "id": self.id,
            "datetime_added": self.datetime_added,
            "geohash": self.geohash,
            "lat": self.get_coords()[0],
            "lng": self.get_coords()[1],
            "scarf_id": self.scarf_id,
            "display_name": self.display_name,
            "age": self.age,
            "movement": self.movement,
            "souvenir_type": self.souvenir_type,
            "souvenir_content": self.souvenir_content,
            "word_list": self.word_list,
            "is_safe": self.is_safe,
        }


class SouvenirAdmin(ModelView):
    model_class = Souvenir

    def is_accessible(self):
        return login.current_user.is_authenticated


if not Souvenir.table_exists():
    Souvenir.create_table()
