# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from datetime import datetime
from datetime import timedelta

from exploreve.models.camp import CampTile


def main():
    """Tests all tiles for consolidation
    """
    tiles = []
    last_week = datetime.now() - timedelta(days=7)

    last_tile = CampTile.create_next()
    last_tile.datetime_added = last_week
    tile = CampTile.get_or_none(x=0, y=0)
    while tile != last_tile:
        tiles.append(tile)
        x, y = tile.get_next_coords()
        tile = CampTile.get_or_none(x=x, y=y)
        if tile is None:
            tile = CampTile(x=x, y=y, datetime_added=last_week)
    tiles.append(last_tile)

    to_delete = []
    for index, tile in enumerate(tiles):
        if (not tile.is_submitted) and (
            tile.datetime_added - datetime.now() < timedelta(days=1)
        ):
            to_delete.append(tile)
            tiles[index] = None
    tiles = [t for t in tiles if t is not None]

    for tile in to_delete:
        print("Deleting", tile)
        tile.delete_instance()

    x, y = tiles[0].x, tiles[0].y
    for tile in tiles:
        print("Moving", tile, "to", (x, y))
        tile.move_to(x, y)
        x, y = tile.get_next_coords()


if __name__ == "__main__":
    main()
