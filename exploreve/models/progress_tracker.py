# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import datetime

import flask_login as login
from flask_admin.contrib.peewee import ModelView
from peewee import AutoField
from peewee import DateTimeField
from peewee import IntegerField
from peewee import Model

from exploreve import explodb


class ProgressTracker(Model):
    class Meta:
        database = explodb  # type: ignore

    id = AutoField()
    immersion_id = IntegerField(null=False)
    date_started = DateTimeField(default=datetime.datetime.utcnow)
    date_lastseen = DateTimeField(default=datetime.datetime.utcnow)
    last_level_done = IntegerField(default=1)

    def get_info(self) -> dict:
        return {
            "id": self.id,
            "immersion_id": self.immersion_id,
            "date_started": self.date_started,
            "date_lastseen": self.date_lastseen,
            "last_level_done": self.last_level_done,
        }


class ProgressTrackerAdmin(ModelView):
    model_class = ProgressTracker

    def is_accessible(self):
        return login.current_user.is_authenticated


if not ProgressTracker.table_exists():
    ProgressTracker.create_table()
