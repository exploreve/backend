# Exploreve Backend

Bienvenue dans le backend du site www.exploreve.fr.

Ce projet utilise le framework Flask. Il contient toute le code de l'api hébergé sur api.exploreve.fr

## Structure du projet

Dans le dossier exploreve vous trouverez:

 - `errors`
    - Des erreurs custom
 - `forms`
    - Les formulaires nécéssaires au vues admin
 - `models`
    - Les models de la base de donnée 
 - `routes`
    - Les routes de l'API
 - `success`
    - Des fonctions custom pour renvoyer un json contant sur toute l'API
 - `templates`
    - Des templates HTML utilisés pour la partie admin et doc
 - `utils`
    - Les fonctions utilitaires
 - `__init__py`
    - L'initialisation des modules et autres composants

## Configurer le projet

Tu auras besoin de Python 3.6+

```shell script
python3 -m venv --prompt $(basename $PWD) venv
```

```shell script
source venv/bin/activate
```

```shell script
pip install -r requirements.txt
```

Si tu n'a pas de .env requis, envoie un mail à technique@exploreve.fr

```shell script
python run.py
```
