FROM python:3.8.3-buster
RUN set -ex && apt-get update 
RUN set -ex && pip install pipenv --upgrade
WORKDIR /www
ADD requirements.txt .
RUN pip install -r requirements.txt
ADD . .
ENV PYTHONDONTWRITEBYTECODE 1
EXPOSE 5000
#ADD .env .
RUN export $(cat .env | xargs)
RUN pip install gunicorn
CMD exec gunicorn --chdir /www --bind :5000 --workers 1 --threads 1 exploreve:application
