# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import datetime

from flask import Blueprint
from flask import request
from peewee import DoesNotExist

from exploreve.errors import NotFoundError
from exploreve.models.progress_tracker import ProgressTracker
from exploreve.success.success import SuccessOutput
from exploreve.utils.decorators import auth_token_required
from exploreve.utils.decorators import validate_required_params

progresstracker_bp = Blueprint("progress_tracker", __name__)

CREATE_TRACKER_PARAMS = {"immersion_id": int}
UPDATE_TRACKER_PARAMS = {"id": int, "last_level_done": int}
UPDATE_TRACKER_OPT_PARAMS = {"immersion_id": int}


@progresstracker_bp.route("/progress/trackers/", methods=["POST"])
@auth_token_required
@validate_required_params(CREATE_TRACKER_PARAMS)
def create_tracker_entry():
    """Create a new progress tracker
    @@@
    #### JSON args
    | args | nullable | type | remark |
    |--------|--------|--------|--------|
    |    immersion_id    |    false    |    int   |    immersion id    |
    #### return JSON
    ```json
    {
        "success": true,
        "code": 200,
        "new_tracker": {
            "id": 1,
            "date_started": "Fri, 27 Mar 2020 20:00:00 GMT",
            "date_lastseen": "Fri, 27 Mar 2020 20:00:00 GMT",
            "immersion_id": 1,
            "last_level_done": 1
        }
    }
    ```
    @@@
    """
    data = request.get_json()
    new_tracker = ProgressTracker(immersion_id=data["immersion_id"], last_level_done=1)
    new_tracker.save()
    return SuccessOutput("new_tracker", new_tracker.get_info(), status_code=201)


@progresstracker_bp.route("/progress/trackers/", methods=["PATCH"])
@auth_token_required
@validate_required_params(UPDATE_TRACKER_PARAMS)
def update_tracker_entry():
    """Update a progress tracker
    @@@
    #### JSON args
    | args | optional | type | remark |
    |--------|--------|--------|--------|
    |    id    |    false    |    int   |    progress_tracker id    |
    |    last_level_done    |    false    |    int   |    Last level done    |
    | immersion_id | true | int | immersion identifier |
    #### return JSON
    ##### On success:
    ```json
    {
        "success": true,
        "code": 200,
        "updated_tracker": {
            "id": 1,
            "date_started": "Fri, 27 Mar 2020 20:00:00 GMT",
            "date_lastseen": "Fri, 27 Mar 2020 20:00:00 GMT",
            "immersion_id": 1,
            "last_level_done": 2
        }
    }
    ```
    ##### On error:
    ```json
    {
      "code": 404,
      "error": {
        "message": "tracker id {} not found",
        "name": "Not Found Error",
        "solution": "Try again",
        "type": "NotFoundError"
      },
      "success": false
    }
    ```
    @@@
    """
    data = request.get_json()
    try:
        tracker = ProgressTracker.get(data["id"])
    except DoesNotExist:
        raise NotFoundError(
            "tracker id {} not found".format(data["id"]),
            "Check your request and try again",
        )

    tracker.last_level_done = data["last_level_done"]
    if "immersion_id" in data:
        tracker.immersion_id = data["immersion_id"]
    tracker.date_lastseen = datetime.datetime.utcnow()
    tracker.save()
    return SuccessOutput("updated_tracker", tracker.get_info())


@progresstracker_bp.route("/progress/trackers/<int:id>", methods=["GET"])
@auth_token_required
def get_progress_tracker(id):
    """Create progress tracker by id
    @@@
    #### URL args
    | args | nullable | type | remark |
    |--------|--------|--------|--------|
    |    id    |    false    |    int   |    progress_tracker id    |
    #### return JSON
    ```json
    {
        "success": true,
        "code": 200,
        "progress_tracker": {
            "id": 1,
            "date_started": "Fri, 27 Mar 2020 20:00:00 GMT",
            "date_lastseen": "Fri, 27 Mar 2020 20:00:00 GMT",
            "immersion_id": 1,
            "last_level_done": 2
        }
    }
    ```
    #### On error:
    ```json
    {
        "success": false,
        "code": 404,
        "error": {
            "message": "Progress tracker with id X not found",
            "name": "Not Found Error",
            "solution": "Try again",
            "type": "NotFoundError"
        }
    }
    ```
    @@@
    """
    try:
        progress_tracker = ProgressTracker.get(id)
    except ProgressTracker.DoesNotExist:
        raise NotFoundError(
            "Progress tracker with id {} not found".format(id), "Try again"
        )
    return SuccessOutput("progress_tracker", progress_tracker.get_info())
