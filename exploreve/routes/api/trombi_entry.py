# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint

from exploreve.models.trombi_entry import TrombiEntry
from exploreve.success.success import SuccessOutput
from exploreve.utils.decorators import auth_token_required

trombi_bp = Blueprint("trombi", __name__)


@trombi_bp.route("/trombi", methods=["GET"])
@auth_token_required
def get_all_trombi():
    """Get all trombinoscope entries
    @@@
    #### return JSON
    ```json
    {
        "success": true,
        "code": 200,
        "trombi": [
            {
                "id": 1,
                "display_name": "Jules Lasne",
                "creation_date": "Fri, 27 Mar 2020 20:00:00 GMT",
                "last_modified": "Fri, 27 Mar 2020 21:00:00 GMT",
                "image_url": "https://cdn.exploreve.fr/content/carte/fantome.svg",
                "movement": "SGDF",
                "roles": ["x", "y"],
            },
            ...
        ]
    }
    ```
    @@@
    """
    all_trombi = []
    for trombi in TrombiEntry.select():
        all_trombi.append(trombi.get_info())
    return SuccessOutput("trombi", all_trombi)
