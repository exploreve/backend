# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from collections import Counter
from itertools import chain
from math import floor
from statistics import mean
from statistics import median

from exploreve.models.souvenir import Souvenir
from exploreve.utils.stats._per_day import get_stats_per_day


def _get_souvenir_wordcloud_stats():
    souvenirs = (
        s for s in Souvenir.select().where(Souvenir.is_safe == True)  # noqa: E712
    )  # is operator does not work

    words_list = (souvenir.word_list for souvenir in souvenirs)

    words = chain.from_iterable(words_list)

    counts = Counter(words)

    most_used_3 = counts.most_common(3)
    num_different_words = len(counts)
    num_words = sum(counts.values())
    ret = {
        "top_3": most_used_3,
        "number_different_words": num_different_words,
        "total": num_words,
    }
    return ret


def _get_souvenir_age_stats():
    souvenir_ages = [s.age for s in Souvenir.select(Souvenir.age)]
    c = Counter(souvenir_ages)
    top_3_ages = c.most_common(3)
    num_diff_ages = len(c)
    num_ages = sum(c.values())
    mean_age = floor(mean(souvenir_ages))
    median_age = floor(median(souvenir_ages))
    ret = {
        "top_3": top_3_ages,
        "num_different": num_diff_ages,
        "total": num_ages,
        "mean": mean_age,
        "median": median_age,
    }
    return ret


def _get_souvenir_movement_stats():
    souvenir_movement = [s.movement for s in Souvenir.select(Souvenir.movement)]
    c = Counter(souvenir_movement)
    top_3_movements = c.most_common(3)
    ret = {"top_3": top_3_movements}
    return ret


def _get_souvenir_content_stats():
    souvenir_types = [s.souvenir_type for s in Souvenir.select(Souvenir.souvenir_type)]

    c = Counter(souvenir_types)
    top_3_types = c.most_common(3)

    souvenir_contents_sizes = [
        len(s.souvenir_content)
        for s in Souvenir.select(Souvenir.souvenir_content).where(
            Souvenir.souvenir_type == "text"
        )
    ]  # noqa: E712

    mean_size = floor(mean(souvenir_contents_sizes))
    median_size = floor(median(souvenir_contents_sizes))

    souvenir_url_domains = []
    for s in Souvenir.select(Souvenir.id, Souvenir.souvenir_content).where(
        Souvenir.souvenir_type == "url"
    ):
        try:
            a = s.souvenir_content.split("/")[2]
            souvenir_url_domains.append(a)
        except IndexError:
            continue

    c = Counter(souvenir_url_domains)
    top_3_domains = c.most_common(3)
    num_diff_domains = len(c)
    num_domains = sum(c.values())

    ret = {
        "types": top_3_types,
        "text": {"mean_size": mean_size, "median_size": median_size},
        "url": {
            "top_3": top_3_domains,
            "num_different": num_diff_domains,
            "total": num_domains,
        },
    }
    return ret


def get_souvenirs_stats():
    total_entries = Souvenir.select().count()
    days, mean_per_day, median_per_day = get_stats_per_day("souvenir")
    ret = {
        "word_cloud": _get_souvenir_wordcloud_stats(),
        "days": {"per": days, "mean": mean_per_day, "median": median_per_day},
        "age": _get_souvenir_age_stats(),
        "movement": _get_souvenir_movement_stats(),
        "count": total_entries,
        "contents": _get_souvenir_content_stats(),
    }
    return ret
