# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from math import floor
from statistics import mean
from statistics import median

from exploreve import explodb


def get_stats_per_day(table_name: str, dt_row_name: str = "datetime_added"):
    """
    Fair warning, You should NEVER execute raw sql like this.
    It could lead to SQL injections and mess up the database

    We authorized ourselves to do this because it is static code, the function calling this is a GET with
    no parametters so we are not at risk here

    You can learn more about SQL injections here https://www.w3schools.com/sql/sql_injection.asp
    """
    cursor = explodb.execute_sql(
        f"SELECT count(*) cnt, date_trunc('day', {dt_row_name}) dt FROM {table_name} GROUP BY dt ORDER BY dt;"
    )
    days = []
    numbers = []
    for row in cursor.fetchall():
        r = {"count": row[0], "date": row[1].strftime("%A %d %B %Y")}
        numbers.append(row[0])
        days.append(r)

    mean_per_day = floor(mean(numbers))
    median_per_day = floor(median(numbers))

    return days, mean_per_day, median_per_day


def get_stats_per_day_per_immersion(immersion_id: int):
    cursor = explodb.execute_sql(
        f"SELECT count(*) cnt, date_trunc('day', date_started) dt FROM progresstracker "
        f"WHERE immersion_id = {immersion_id} GROUP BY dt ORDER BY dt;"
    )
    days = []
    numbers = []
    for row in cursor.fetchall():
        r = {"count": row[0], "date": row[1].strftime("%A %d %B %Y")}
        numbers.append(row[0])
        days.append(r)

    mean_per_day = floor(mean(numbers))
    median_per_day = floor(median(numbers))

    return days, mean_per_day, median_per_day


def get_stats_per_day_per_immersion_for_level(immersion_id: int, level_id: int):
    cursor = explodb.execute_sql(
        f"SELECT count(*) cnt, date_trunc('day', date_started) dt FROM progresstracker "
        f"WHERE immersion_id = {immersion_id} AND last_level_done >= {level_id} GROUP BY dt ORDER BY dt;"
    )
    days = []
    numbers = []
    for row in cursor.fetchall():
        r = {"count": row[0], "date": row[1].strftime("%A %d %B %Y")}
        numbers.append(row[0])
        days.append(r)

    mean_per_day = floor(mean(numbers))
    median_per_day = floor(median(numbers))

    return days, mean_per_day, median_per_day
