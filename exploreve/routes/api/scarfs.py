# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint
from flask import request
from peewee import fn

from exploreve.errors import NotFoundError
from exploreve.models.scarf import Scarf
from exploreve.success.success import SuccessOutput
from exploreve.utils.color import validate_color
from exploreve.utils.decorators import auth_token_required
from exploreve.utils.decorators import validate_required_params

scarf_bp = Blueprint("scarfs", __name__)

# Required keys to create a scarf
REQUIRED_KEYS_SCARF_COLOR = {
    "border_color": str,
    "background_color": str,
    "knot_color": str,
}


@scarf_bp.route("/scarfs/", methods=["GET"])
@auth_token_required
def get_all_scarfs():
    """Get all scarfs
    @@@
    #### return JSON
    ```json
    {
        "success": true,
        "code": 200,
        "scarfs": [
            {
                "id": 1,
                "border_color": "#000000",
                "background_color": "#000000",
                "knot_color": "#000000",
                "url": "/scarfs/1",
                "datetime_added": "Fri, 27 Mar 2020 20:00:00 GMT",
            },
            ...
        ]
    }
    ```
    @@@
    """
    scarf_list = []
    for scarf in Scarf.select():
        scarf_list.append(scarf.get_info())
    return SuccessOutput("scarfs", scarf_list)


@scarf_bp.route("/scarfs/random/<int:nb>", methods=["GET"])
@auth_token_required
def get_random_scarfs_nb(nb):
    """Get a number of random scarfs
    @@@
    #### URL args
    | args | nullable | type | remark |
    |--------|--------|--------|--------|
    |    nb    |    false    |    int   |    Number of random scarfs you want    |
    #### return JSON
    ```json
    {
        "success": true,
        "code": 200,
        "scarfs": [
            {
                "id": 1,
                "border_color": "#000000",
                "background_color": "#000000",
                "knot_color": "#000000",
                "url": "/scarfs/1",
                "datetime_added": "Fri, 27 Mar 2020 20:00:00 GMT",
            },
            ...
        ]
    }
    ```
    @@@
    """
    scarf_list = []
    scarfs = Scarf.select().order_by(fn.Random()).limit(nb)
    for scarf in scarfs:
        scarf_list.append(scarf.get_info())
    return SuccessOutput("scarfs", scarf_list)


@scarf_bp.route("/scarfs/<int:id>/", methods=["GET"])
@auth_token_required
def get_scarf(id):
    """Get a scarf by id
    @@@
    #### URL args
    | args | nullable | type | remark |
    |--------|--------|--------|--------|
    |    id    |    false    |    int   |    ID of the scarf    |
    #### return JSON
    ```json
    {
        "success": true,
        "code": 200,
        "scarf": {
            "id": 1,
            "border_color": "#000000",
            "background_color": "#000000",
            "knot_color": "#000000",
            "url": "/scarfs/1",
            "datetime_added": "Fri, 27 Mar 2020 20:00:00 GMT",
        }
    }
    ```
    #### On error:
    ```json
    {
        "success": false,
        "code": 404,
        "error": {
            "message": "Scarf does not exist",
            "name": "Not Found Error",
            "solution": "Please create it",
            "type": "NotFoundError"
        }
    }
    ```
    @@@
    """
    try:
        scarf = Scarf.get(id=id)
    except Scarf.DoesNotExist:
        raise NotFoundError("Scarf does not exist", "Please create it")
    scarf_info = scarf.get_info()
    return SuccessOutput("scarf", scarf_info)


@scarf_bp.route("/scarfs/", methods=["POST"])
@auth_token_required
@validate_required_params(REQUIRED_KEYS_SCARF_COLOR)
def post_scarf():
    """Create a new scarf
    @@@
    #### JSON args
    | args | nullable | type | remark |
    |--------|--------|--------|--------|
    |    border_color    |    false    |    str   |    Hex value for the border of the scarf    |
    |    background_color    |    false    |    str   |    Hex value for the background of the scarf    |
    |    knot_color    |    false    |    str   |    Hex value for the knot of the scarf    |
    #### return JSON
    ```json
    {
        "success": true,
        "code": 200,
        "scarf": {
            "id": 1,
            "border_color": "#000000",
            "background_color": "#000000",
            "knot_color": "#000000",
            "url": "/scarfs/1",
            "datetime_added": "Fri, 27 Mar 2020 20:00:00 GMT",
        }
    }
    ```
    @@@
    """
    data = request.get_json()
    # Validate the color and then create the scarf
    scarf = Scarf(
        border_color=validate_color(data["border_color"]),
        background_color=validate_color(data["background_color"]),
        knot_color=validate_color(data["knot_color"]),
    )
    scarf.save()
    scarf_info = scarf.get_info()
    return SuccessOutput("scarf", scarf_info, status_code=201)


@scarf_bp.route("/scarfs/<int:id>/", methods=["POST"])
@auth_token_required
@validate_required_params(REQUIRED_KEYS_SCARF_COLOR)
def put_scarf(id):
    """Update a scarf
    @@@
    #### URL args
    | args | nullable | type | remark |
    |--------|--------|--------|--------|
    |    id    |    false    |    int   |    Scarf ID    |

    #### JSON args
    | args | nullable | type | remark |
    |--------|--------|--------|--------|
    |    border_color    |    false    |    str   |    Hex value for the border of the scarf    |
    |    background_color    |    false    |    str   |    Hex value for the background of the scarf    |
    |    knot_color    |    false    |    str   |    Hex value for the knot of the scarf    |
    #### return JSON
    ```json
    {
        "success": true,
        "code": 200,
        "scarf": {
            "id": 1,
            "border_color": "#000000",
            "background_color": "#000000",
            "knot_color": "#000000",
            "url": "/scarfs/1",
            "datetime_added": "Fri, 27 Mar 2020 20:00:00 GMT",
        }
    }
    ```
    #### On error:
    ```json
    {
        "success": false,
        "code": 404,
        "error": {
            "message": "Scarf does not exist",
            "name": "Not Found Error",
            "solution": "Please create it",
            "type": "NotFoundError"
        }
    }
    ```
    @@@
    """
    data = request.get_json()
    try:
        scarf = Scarf.get(id=id)
    except Scarf.DoesNotExist:
        raise NotFoundError("Scarf does not exist", "Please create it")

    scarf.border_color = validate_color(data["border_color"])
    scarf.background_color = validate_color(data["background_color"])
    scarf.knot_color = validate_color(data["knot_color"])

    scarf.save()

    scarf_info = scarf.get_info()
    return SuccessOutput("scarf", scarf_info)
