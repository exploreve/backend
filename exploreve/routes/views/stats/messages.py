# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint
from flask import render_template

from exploreve.utils.color import get_random_color_sets
from exploreve.utils.stats.messages import _get_messages_age_stats
from exploreve.utils.stats.messages import _get_messages_movement_stats

# from exploreve.utils.stats.messages import _get_nb_times_read_stats

stats_view_message_bp = Blueprint("stats_view_messages", __name__)


@stats_view_message_bp.route("/stats/view/messages/other", methods=["GET"])
def messages_other_stats():
    a = _get_messages_age_stats()
    print(a)
    total_different_ages = a["num_different"]
    total_ages = a["total"]
    mean_age = a["mean"]
    median_age = a["median"]
    labels_age = [a["top_3"][0][0], a["top_3"][1][0], a["top_3"][2][0]]
    data_age = [a["top_3"][0][1], a["top_3"][1][1], a["top_3"][2][1]]
    color_sets_ages = get_random_color_sets(3)
    ages = {
        "total_different": total_different_ages,
        "total": total_ages,
        "mean": mean_age,
        "median": median_age,
        "labels": labels_age,
        "data": data_age,
        "color_sets": color_sets_ages,
    }

    m = _get_messages_movement_stats()
    print(m)
    labels_movement = [m["top_3"][0][0], m["top_3"][1][0], m["top_3"][2][0]]
    data_movement = [m["top_3"][0][1], m["top_3"][1][1], m["top_3"][2][1]]
    color_sets_movement = get_random_color_sets(3)
    movements = {
        "labels": labels_movement,
        "data": data_movement,
        "color_sets": color_sets_movement,
    }

    return render_template("stats/messages.html", ages=ages, movements=movements)
