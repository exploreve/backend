import datetime
from typing import Any
from typing import Dict

import flask_login as login
from flask_admin.contrib.peewee import ModelView
from peewee import AutoField
from peewee import BooleanField
from peewee import CharField
from peewee import DateTimeField
from peewee import IntegerField
from peewee import Model
from peewee import TextField

from exploreve import explodb


class Message(Model):
    """A message"""

    class Meta:
        database = explodb  # type: ignore

    id = AutoField()
    datetime_added = DateTimeField(default=datetime.datetime.utcnow)
    # Displayed name of the person who wrote the souvenir
    display_name = CharField(null=False)
    age = IntegerField(null=False)
    movement = CharField(
        null=False,
        choices=[
            ("SGDF", "SGDF"),
            ("EEDF", "EEDF"),
            ("EEUDF", "EEUDF"),
            ("EEIF", "EEIF"),
            ("SMF", "SMF"),
            ("EDLN", "EDLN"),
            ("AGSE", "AGSE"),
            ("SUF", "SUF"),
            ("FEE", "FEE"),
            ("Ecl Neutres", "Ecl Neutres"),
            ("autre", "autre"),
        ],
    )

    job = CharField(null=False)
    job_reason = TextField(null=False)

    place = CharField(null=False)
    place_reason = TextField(null=False)

    personality = CharField(null=False)
    personality_reason = TextField(null=False)

    bored_activity = TextField(null=False)

    scouting_activity = TextField(null=False)

    like_about_friends = TextField(null=False)

    nb_times_read = IntegerField(default=0)

    # Just in case we need to moderate
    is_safe = BooleanField(default=True)

    def get_info(self) -> Dict[str, Any]:
        return {
            "id": self.id,
            "datetime_added": self.datetime_added,
            "display_name": self.display_name,
            "age": self.age,
            "movement": self.movement,
            "job": self.job,
            "job_reason": self.job_reason,
            "place": self.place,
            "place_reason": self.place_reason,
            "personality": self.personality,
            "personality_reason": self.personality_reason,
            "bored_activity": self.bored_activity,
            "scouting_activity": self.scouting_activity,
            "like_about_friends": self.like_about_friends,
            "is_safe": self.is_safe,
            "nb_times_read": self.nb_times_read,
        }


class MessageAdmin(ModelView):
    model_class = Message

    def is_accessible(self):
        return login.current_user.is_authenticated


if not Message.table_exists():
    Message.create_table()
