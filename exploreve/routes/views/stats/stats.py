# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint
from flask import flash
from flask import redirect
from flask import render_template
from flask import url_for
from flask_login import login_required

from exploreve.models.progress_tracker import ProgressTracker
from exploreve.utils.color import get_4_random_colors
from exploreve.utils.stats._per_day import get_stats_per_day
from exploreve.utils.stats.immersions import get_immersion_stats
from exploreve.utils.stats.messages import get_messages_stats
from exploreve.utils.stats.newsletter import get_newsletter_stats
from exploreve.utils.stats.scarfs import get_scarfs_stats
from exploreve.utils.stats.souvenirs import get_souvenirs_stats
from exploreve.utils.stats.tiles import get_tiles_stats

stats_view_bp = Blueprint("stats_view", __name__)


def _render_graph_per_day(stats, title):
    labels = []
    data = []
    for e in stats["days"]["per"]:
        labels.append(e["date"])
        data.append(e["count"])
    (
        border_color,
        background_color,
        pointborder_color,
        pointhover_color,
    ) = get_4_random_colors()
    return render_template(
        "stats/_per_day.html",
        labels=labels,
        data=data,
        title=title,
        border_color=border_color,
        background_color=background_color,
        pointhover_color=pointhover_color,
        pointborder_color=pointborder_color,
        mean_per_day=stats["days"]["mean"],
        median_per_day=stats["days"]["median"],
    )


@stats_view_bp.route(
    "/stats/view/multiperday/immersions/<int:immersion_id>", methods=["GET"]
)
@login_required
def view_per_day_stats_immersions(immersion_id):
    datasets = []
    labels = []
    if immersion_id == 1:
        resp = get_immersion_stats(immersion_id=1, end_level=9, approx_end_level=8)
    elif immersion_id == 2:
        resp = get_immersion_stats(immersion_id=2, end_level=7, approx_end_level=5)
    else:
        flash(f"Unknown immersion id: {immersion_id}", "danger")
        return redirect(url_for("stats_view.stats_view_home"))
    for d in resp["all_players"]["days"]["per"]:
        labels.append(d["date"])

    for t in [
        ("all_players", resp["all_players"]["count"]),
        ("finishers", resp["finishers"]["count"]),
        ("almost_finishers", resp["almost_finishers"]["count"]),
    ]:
        (
            border_color,
            background_color,
            pointborder_color,
            pointhover_color,
        ) = get_4_random_colors()
        datasets.append(
            {
                "title": t[0],
                "data": [d["count"] for d in resp[t[0]]["days"]["per"]],
                "background_color": background_color,
                "border_color": border_color,
                "pointborder_color": pointborder_color,
                "pointhover_color": pointhover_color,
                "count": t[1],
            }
        )
    return render_template(
        "stats/_multi_per_day.html", labels=labels, datasets=datasets
    )


@stats_view_bp.route("/stats/view/perday/<table>", methods=["GET"])
@login_required
def view_per_day_stats(table: str):
    switch = {
        "tiles": get_tiles_stats,
        "scarfs": get_scarfs_stats,
        "messages": get_messages_stats,
        "newsletter": get_newsletter_stats,
        "souvenirs": get_souvenirs_stats,
    }
    try:
        return _render_graph_per_day(
            switch[table](),
            f"{table.capitalize()} per day (Total: {switch[table]()['count']})",
        )
    except KeyError:
        flash(f"Unknown table for stats per day: {table}", "danger")
        return redirect(url_for("stats_view.stats_view_home"))


@stats_view_bp.route("/stats/view", methods=["GET"])
@login_required
def stats_view_home():
    (
        border_color,
        background_color,
        pointborder_color,
        pointhover_color,
    ) = get_4_random_colors()
    days, mean_per_day, median_per_day = get_stats_per_day(
        table_name="progresstracker", dt_row_name="date_started"
    )
    # TODO: Add mean and median lines
    #  https://stackoverflow.com/questions/42691873/draw-horizontal-line-on-chart-in-chart-js-on-v2
    return render_template(
        "stats/index.html",
        days=days,
        mean_per_day=mean_per_day,
        median_per_day=median_per_day,
        border_color=border_color,
        background_color=background_color,
        pointhover_color=pointhover_color,
        pointborder_color=pointborder_color,
        total=ProgressTracker.select().count(),
    )
