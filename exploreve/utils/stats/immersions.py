# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import statistics
import time
from math import floor

from exploreve.models.progress_tracker import ProgressTracker
from exploreve.utils.stats._per_day import get_stats_per_day_per_immersion
from exploreve.utils.stats._per_day import get_stats_per_day_per_immersion_for_level


def get_stats_for_tracker_list(tracker_list):
    times_played_in_seconds = []
    for tracker in tracker_list:
        diff = tracker.date_lastseen - tracker.date_started
        times_played_in_seconds.append(diff.seconds)
    player_count = len(times_played_in_seconds)
    mean_seconds = statistics.mean(times_played_in_seconds)
    mean_hhmmss = time.strftime("%H:%M:%S", time.gmtime(mean_seconds))
    median_seconds = statistics.median(times_played_in_seconds)
    median_hhmmss = time.strftime("%H:%M:%S", time.gmtime(median_seconds))
    ret = {
        "mean_hhmmss": mean_hhmmss,
        "median_hhmmss": median_hhmmss,
        "mean_seconds": floor(mean_seconds),
        "median_seconds": floor(median_seconds),
    }
    return ret, player_count


def get_immersion_stats(
    immersion_id: int, end_level: int, approx_end_level: int
) -> dict:
    """
    :param immersion_id: The id of the immersion
    :param end_level: The level_id where the code for the next immersion is given
    :param approx_end_level: The approximate end level id
        (the level where a player is considered to have played the entire immersion but not gotten the code)
    """
    finishers = ProgressTracker.select().where(
        ProgressTracker.last_level_done >= end_level,
        ProgressTracker.immersion_id == immersion_id,
    )
    finishers_stats, finishers_count = get_stats_for_tracker_list(finishers)

    all_players = ProgressTracker.select().where(
        ProgressTracker.immersion_id == immersion_id
    )
    all_players_stats, all_players_count = get_stats_for_tracker_list(all_players)

    almost_finishers = ProgressTracker.select().where(
        ProgressTracker.last_level_done >= approx_end_level,
        ProgressTracker.immersion_id == immersion_id,
    )
    almost_finishers_stats, almost_finishers_count = get_stats_for_tracker_list(
        almost_finishers
    )

    levels = []
    for tracker in finishers:
        levels.append(tracker.last_level_done)
    most_level = statistics.mode(levels)

    days, mean_per_day, median_per_day = get_stats_per_day_per_immersion(immersion_id)
    (
        done_days,
        done_mean_per_day,
        done_median_per_day,
    ) = get_stats_per_day_per_immersion_for_level(immersion_id, end_level)
    (
        almost_done_days,
        almost_done_mean_per_day,
        almost_done_median_per_day,
    ) = get_stats_per_day_per_immersion_for_level(immersion_id, approx_end_level)

    return {
        "immersion_id": immersion_id,
        "total_players": len(all_players),
        "most_seen_level": most_level,
        "finishers": {
            "detail": f"These stats are about all the players that finished the immersion {immersion_id}",
            "timings": finishers_stats,
            "count": finishers_count,
            "days": {
                "per": done_days,
                "mean": done_mean_per_day,
                "median": done_median_per_day,
            },
        },
        "almost_finishers": {
            "detail": "These stats are about all the player that made it to the end or almost "
            "(didn't get to the next immersion code but played all the games)",
            "timings": almost_finishers_stats,
            "count": almost_finishers_count,
            "days": {
                "per": almost_done_days,
                "mean": almost_done_mean_per_day,
                "median": almost_done_median_per_day,
            },
        },
        "all_players": {
            "detail": f"These stats are about all the players that started the immersion {immersion_id}",
            "timings": all_players_stats,
            "count": all_players_count,
            "days": {"per": days, "mean": mean_per_day, "median": median_per_day},
        },
    }
