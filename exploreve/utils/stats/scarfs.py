# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import statistics
from math import floor

from exploreve.models.scarf import Scarf
from exploreve.utils.stats._per_day import get_stats_per_day


def get_scarfs_stats():
    scarfs = Scarf.select()

    knots = [k.knot_color for k in scarfs]
    borders = [k.border_color for k in scarfs]
    backgrounds = [k.background_color for k in scarfs]

    most_knot = statistics.mode(knots)
    most_border = statistics.mode(borders)
    most_background = statistics.mode(backgrounds)

    knots_as_ints = [int(k[1:], 16) for k in knots]
    borders_as_ints = [int(k[1:], 16) for k in borders]
    backgrounds_as_ints = [int(k[1:], 16) for k in backgrounds]

    mean_knot = "#" + str(hex(floor(statistics.mean(knots_as_ints))))[2:]
    median_knot = "#" + str(hex(floor(statistics.median(knots_as_ints))))[2:]

    mean_border = "#" + str(hex(floor(statistics.mean(borders_as_ints))))[2:]
    median_border = "#" + str(hex(floor(statistics.median(borders_as_ints))))[2:]

    mean_background = "#" + str(hex(floor(statistics.mean(backgrounds_as_ints))))[2:]
    median_background = (
        "#" + str(hex(floor(statistics.median(backgrounds_as_ints))))[2:]
    )

    days, mean_per_day, median_per_day = get_stats_per_day("scarf")

    ret = {
        "count": len(scarfs),
        "most_of": {
            "knot": most_knot,
            "border": most_border,
            "background": most_background,
        },
        "median": {
            "knot": median_knot,
            "border": median_border,
            "background": median_background,
        },
        "mean": {
            "knot": mean_knot,
            "border": mean_border,
            "background": mean_background,
        },
        "days": {"per": days, "mean": mean_per_day, "median": median_per_day},
    }
    return ret
