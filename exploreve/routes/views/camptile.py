# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint
from flask import redirect
from flask import render_template
from flask import request
from flask import url_for
from flask_login import login_required

from exploreve.models.camp import CampTile

camptile_view_bp = Blueprint("camptile_views", __name__)

TILES_PER_AGE = 9


def camptile_moderate_redirect(state: str):
    if state == "all":
        return redirect(url_for("camptile_views.all"))
    if state == "safe":
        return redirect(url_for("camptile_views.safe"))
    if state == "unsafe":
        return redirect(url_for("camptile_views.unsafe"))
    if state == "unreviewed":
        return redirect(url_for("camptile_views.unreviewed"))
    if state == "reviewed":
        return redirect(url_for("camptile_views.reviewed"))
    return redirect(url_for("camptile_views.all"))


@camptile_view_bp.route("/admin/camptile/moderate/all", methods=["GET"])
@login_required
def all():
    tile_list = []
    for tile in CampTile.select().order_by(CampTile.id):
        tile_info = tile.get_info()
        tile_info["img_url"] = tile.get_url()
        tile_list.append(tile_info)
    return render_template("view_tiles.html", tile_list=tile_list, state="all")


@camptile_view_bp.route("/admin/camptile/moderate/reviewed", methods=["GET"])
@login_required
def reviewed():
    tile_list = []
    for tile in (
        CampTile.select()
        .where(CampTile.is_reviewed == True)  # noqa: E712
        .order_by(CampTile.id)
    ):
        tile_info = tile.get_info()
        tile_info["img_url"] = tile.get_url()
        tile_list.append(tile_info)
    return render_template("view_tiles.html", tile_list=tile_list, state="reviewed")


@camptile_view_bp.route("/admin/camptile/moderate/unreviewed", methods=["GET"])
@login_required
def unreviewed():
    tile_list = []
    for tile in (
        CampTile.select()
        .where(CampTile.is_reviewed == False)  # noqa: E712
        .order_by(CampTile.id)
        .paginate(1, TILES_PER_AGE)
    ):
        tile_info = tile.get_info()
        tile_info["img_url"] = tile.get_url()
        tile_list.append(tile_info)

    return render_template("view_tiles.html", tile_list=tile_list, state="unreviewed")


def moderate_camptile(id: str, is_safe: bool):
    (
        CampTile.update({CampTile.is_reviewed: True, CampTile.is_safe: is_safe})
        .where(CampTile.id == id)
        .execute()
    )


@camptile_view_bp.route("/admin/camptile/moderate/safe", methods=["GET", "POST"])
@login_required
def safe():
    if request.method == "POST":
        id = request.form.get("id")
        callback_state = request.form.get("state")
        if id:
            moderate_camptile(id, True)
            return camptile_moderate_redirect(callback_state)

    tile_list = []
    for tile in (
        CampTile.select()
        .where(CampTile.is_safe == True)  # noqa: E712
        .order_by(CampTile.id)
    ):
        tile_info = tile.get_info()
        tile_info["img_url"] = tile.get_url()
        tile_list.append(tile_info)
    return render_template("view_tiles.html", tile_list=tile_list, state="safe")


@camptile_view_bp.route("/admin/camptile/moderate/unsafe", methods=["GET", "POST"])
@login_required
def unsafe():
    if request.method == "POST":
        id = request.form.get("id")
        callback_state = request.form.get("state")
        if id:
            moderate_camptile(id, False)
            return camptile_moderate_redirect(callback_state)

    tile_list = []
    for tile in (
        CampTile.select()
        .where(CampTile.is_safe == False)  # noqa: E712
        .order_by(CampTile.id)
    ):
        tile_info = tile.get_info()
        tile_info["img_url"] = tile.get_url()
        tile_list.append(tile_info)
    return render_template("view_tiles.html", tile_list=tile_list, state="unsafe")
