# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import os

import boto3
import peewee
import sentry_sdk
from boto3.session import Session
from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import render_template
from flask_admin import Admin
from flask_cors import CORS
from flask_login import LoginManager
from flask_pagedown import PageDown
from sentry_sdk import last_event_id
from sentry_sdk.integrations.flask import FlaskIntegration

from exploreve.utils.docs import ApiDoc

# Allows for an easy import like this: `from exploreve import X`

__all__ = [
    "application",
    "explodb",
    "cellar_client",
    "cellar_bucket",
    "cellar_bucket_name",
    "MAILING_LIST",
]

# Required env vars to start the server
REQUIRED_ENV_VARS = [
    "FLASK_SECRET_KEY",
    "POSTGRESQL_ADDON_DB",
    "POSTGRESQL_ADDON_USER",
    "POSTGRESQL_ADDON_PASSWORD",
    "POSTGRESQL_ADDON_HOST",
    "POSTGRESQL_ADDON_PORT",
    "CELLAR_ADDON_KEY_ID",
    "CELLAR_ADDON_KEY_SECRET",
    "CELLAR_ADDON_HOST",
    "CELLAR_BUCKET_NAME",
    "MAILING_LIST_HOST",
    "MAILING_LIST_TOKEN",
    "MAILING_LIST_ID",
    "EXPLOREVE_API_AUTH_TOKEN",
]

BACKEND_ROOT = os.path.join(
    os.path.dirname(__file__), "../"
)  # refers to application_top

# Gets the content of the VERSION file
with open(BACKEND_ROOT + "VERSION", "r") as f:
    VERSION = f.readline().strip()

dotenv_path = os.path.join(BACKEND_ROOT, ".env")
load_dotenv(dotenv_path)

# Raises an error if a required env var isn't present
for item in REQUIRED_ENV_VARS:
    if item not in os.environ:
        raise EnvironmentError(
            f"{item} is not set in the server's environment or .env file. It is required"
        )

# Initializes the Sentry SDK
sentry_sdk.init(
    dsn="https://1c76e2b19a9f4af1811fc33ea75b5f90@sentry.exploreve.fr/2",
    integrations=[FlaskIntegration()],
    release="Backend@" + VERSION,
)

# Create the base flask app
application = Flask(__name__)
application.debug = bool(os.getenv("FLASK_DEBUG", False))
application.secret_key = os.getenv("FLASK_SECRET_KEY")

# Initializes and allows the CORS
CORS(application, allow_headers="*", origins="*", headers="*", expose_headers="*")


# Configures the 500 errors to be handled by sentry's popup
@application.errorhandler(500)
def server_error_handler(error):
    return render_template("sentry_500.html", sentry_event_id=last_event_id()), 500


# Initialize PageDown for the article creation
pagedown = PageDown(application)

# Configure the style of the admin page
application.config["FLASK_ADMIN_SWATCH"] = "lumen"

# Stores some vars in a dict used for the mailing list
MAILING_LIST = {
    "ID": os.getenv("MAILING_LIST_ID"),
    "TOKEN": os.getenv("MAILING_LIST_TOKEN"),
    "HOST": os.getenv("MAILING_LIST_HOST"),
}

# Initializes the PostgreSQL DB
explodb = peewee.PostgresqlDatabase(
    database=os.getenv("POSTGRESQL_ADDON_DB"),
    user=os.getenv("POSTGRESQL_ADDON_USER"),
    password=os.getenv("POSTGRESQL_ADDON_PASSWORD"),
    host=os.getenv("POSTGRESQL_ADDON_HOST"),
    port=os.getenv("POSTGRESQL_ADDON_PORT"),
)


# Opens the DB connection before each request
@application.before_request
def open_db():
    try:
        explodb.connect()
    except peewee.OperationalError:
        pass


# Closes the DB connection after each request
@application.after_request
def close_db(response):
    if not explodb.is_closed():
        explodb.close()
    return response


# Connects to the session for the cellar
boto_session = Session()
cellar_params = {
    "service_name": "s3",
    "aws_access_key_id": os.getenv("CELLAR_ADDON_KEY_ID"),
    "aws_secret_access_key": os.getenv("CELLAR_ADDON_KEY_SECRET"),
    "endpoint_url": "https://" + os.getenv("CELLAR_ADDON_HOST", "localhost"),
}
cellar_client = boto_session.client(**cellar_params)
cellar_res = boto3.resource(**cellar_params)
cellar_bucket_name = os.getenv("CELLAR_BUCKET_NAME")
cellar_bucket = cellar_res.Bucket(cellar_bucket_name)

# Initializes the login manager used for the admin
login_manager = LoginManager()
login_manager.init_app(application)

# API blueprints
from exploreve.routes.api.articles import article_bp
from exploreve.routes.api.camps import camp_bp
from exploreve.routes.api.scarfs import scarf_bp
from exploreve.routes.api.newsletter import newsletter_bp
from exploreve.routes.api.progress_tracker import progresstracker_bp
from exploreve.routes.api.souvenirs import souvenir_bp
from exploreve.routes.api.trombi_entry import trombi_bp
from exploreve.routes.api.hints import hint_bp
from exploreve.routes.api.stats import stats_bp
from exploreve.routes.api.messages import message_bp

# Registers all the routes for API
application.register_blueprint(article_bp)  # type: ignore
application.register_blueprint(camp_bp)  # type: ignore
application.register_blueprint(scarf_bp)  # type: ignore
application.register_blueprint(newsletter_bp)  # type: ignore
application.register_blueprint(progresstracker_bp)  # type: ignore
application.register_blueprint(souvenir_bp)  # type: ignore
application.register_blueprint(trombi_bp)  # type: ignore
application.register_blueprint(hint_bp)  # type: ignore
application.register_blueprint(stats_bp)  # type: ignore
application.register_blueprint(message_bp)  # type: ignore

# Views Blueprints
from exploreve.routes.views.camptile import camptile_view_bp
from exploreve.routes.views.stats.stats import stats_view_bp
from exploreve.routes.views.stats.scarfs import stats_view_scarfs_bp
from exploreve.routes.views.stats.tiles import stats_view_tiles_bp
from exploreve.routes.views.stats.souvenirs import stats_view_souvenir_bp
from exploreve.routes.views.stats.messages import stats_view_message_bp

# Registers all the routes for views
application.register_blueprint(camptile_view_bp)  # type: ignore
application.register_blueprint(stats_view_bp)  # type: ignore
application.register_blueprint(stats_view_scarfs_bp)  # type: ignore
application.register_blueprint(stats_view_tiles_bp)  # type: ignore
application.register_blueprint(stats_view_souvenir_bp)  # type: ignore
application.register_blueprint(stats_view_message_bp)  # type: ignore

application.config["API_DOC_MEMBER"] = [
    "articles",
    "camp",
    "scarfs",
    "newsletter",
    "progress",
    "souvenirs",
    "trombi",
    "hints",
    "messages",
]

application.config["RESTFUL_API_DOC_EXCLUDE"] = []
application.config["API_DOC_ENABLE"] = True
ApiDoc(title="Explorêve API Documentation", version=VERSION, app=application)

from exploreve.models.user import User, UserAdmin
from exploreve.models.article import Article, ArticleAdmin
from exploreve.models.camp import CampTile, CampTileAdmin
from exploreve.models.scarf import Scarf, ScarfAdmin
from exploreve.utils.admin_auth import MyAdminIndexView
from exploreve.models.newsletter_entry import NewsletterEntry, NewsletterEntryAdmin
from exploreve.models.progress_tracker import ProgressTracker, ProgressTrackerAdmin
from exploreve.models.souvenir import Souvenir, SouvenirAdmin
from exploreve.models.trombi_entry import TrombiEntry, TrombiEntryAdmin
from exploreve.models.hint import Hint, HintAdmin
from exploreve.models.message import Message, MessageAdmin

# Creates the Admin manager
admin = Admin(
    application,
    name="Explorêve Admin",
    template_mode="bootstrap3",
    index_view=MyAdminIndexView(),
    base_template="my_master.html",
)

# Registers the views for each table
admin.add_view(UserAdmin(User))
admin.add_view(ArticleAdmin(Article))
admin.add_view(CampTileAdmin(CampTile))
admin.add_view(ScarfAdmin(Scarf))
admin.add_view(NewsletterEntryAdmin(NewsletterEntry))
admin.add_view(ProgressTrackerAdmin(ProgressTracker))
admin.add_view(SouvenirAdmin(Souvenir))
admin.add_view(TrombiEntryAdmin(TrombiEntry))
admin.add_view(HintAdmin(Hint))
admin.add_view(MessageAdmin(Message))

from exploreve.errors import NotFoundError


# Loads the user when a request is done to a protected page
@login_manager.user_loader
def load_user(uid):
    try:
        user = User.get_by_id(uid)
    except NotFoundError:
        return None
    else:
        return user


# Home route
@application.route("/")
def home_route():
    return (
        jsonify(
            {
                "message": "Hello petit curieux ! Bienvenue dans l'API de l'ExploRêve !",
                "version": VERSION,
                "license": """\
Copyright (C) 2020 ExploRêve

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.\
""",
                "source": "https://gitlab.com/exploreve/backend",
                "stats": "https://api.exploreve.fr/stats",
                "documentation": "https://api.exploreve.fr/docs",
            }
        ),
        200,
    )
