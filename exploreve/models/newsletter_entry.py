# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import datetime

import flask_login as login
from flask_admin.contrib.peewee import ModelView
from peewee import AutoField
from peewee import CharField
from peewee import DateTimeField
from peewee import Model

from exploreve import explodb


class NewsletterEntry(Model):
    class Meta:
        database = explodb  # type: ignore

    # Newsletter's entry ID (primary key)
    id = AutoField()
    # The email added to the newsletter
    email = CharField(unique=True)
    # When the email was added to the list
    datetime_added = DateTimeField(default=datetime.datetime.utcnow)


class NewsletterEntryAdmin(ModelView):
    model_class = NewsletterEntry

    def is_accessible(self):
        return login.current_user.is_authenticated


if not NewsletterEntry.table_exists():
    NewsletterEntry.create_table()
