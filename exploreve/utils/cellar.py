# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import logging
from typing import Dict
from typing import Optional
from typing import TypedDict  # noqa

from botocore.exceptions import ClientError

from exploreve import cellar_bucket_name
from exploreve import cellar_client

__all__ = ["create_presigned_url", "create_presigned_post", "PresignedDict"]


def create_presigned_url(object_name: str, expiration=3600) -> Optional[str]:
    """Generate a presigned URL to share a Cellar object

    :param bucket_name: string
    :param object_name: string
    :param expiration: Time in seconds for the presigned URL to remain valid
    :return: Presigned URL as string. If error, returns None.
    """

    # Generate a presigned URL for the Cellar object
    try:
        response = cellar_client.generate_presigned_url(
            "get_object",
            Params={"Bucket": cellar_bucket_name, "Key": object_name},
            ExpiresIn=expiration,
        )
    except ClientError as e:
        logging.error(e)
        return None

    # The response contains the presigned URL
    return response


PresignedDict = TypedDict("PresignedDict", {"url": str, "fields": Dict[str, str]})


def create_presigned_post(object_name: str, expiration=3600) -> Optional[PresignedDict]:
    """Generate a presigned URL Cellar POST request to upload a file

    :param bucket_name: string
    :param object_name: string
    :param fields: Dictionary of prefilled form fields
    :param conditions: List of conditions to include in the policy
    :param expiration: Time in seconds for the presigned URL to remain validmy
    :return: Dictionary with the following keys:
        url: URL to post to
        fields: Dictionary of form fields and values to submit with the POST
    :return: None if error.
    """

    # Generate a presigned Cellar POST URL
    try:
        response = cellar_client.generate_presigned_post(
            cellar_bucket_name, object_name, ExpiresIn=expiration
        )
    except ClientError as e:
        logging.error(e)
        return None

    # The response contains the presigned URL and required fields
    return response
