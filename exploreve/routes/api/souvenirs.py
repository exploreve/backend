# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import json

from flask import Blueprint
from flask import jsonify
from flask import request
from Geohash import encode as gh_encode
from peewee import IntegrityError

from exploreve.models.scarf import Scarf
from exploreve.models.souvenir import Souvenir
from exploreve.success.success import SuccessOutput
from exploreve.utils.constants import MOVEMENT_LIST
from exploreve.utils.decorators import auth_token_required
from exploreve.utils.decorators import validate_required_params
from exploreve.utils.format_souvenir_keywords import clean
from exploreve.utils.word_cloud import get_words_by_weight

souvenir_bp = Blueprint("souvenirs", __name__)

REQUIRED_ARGS_SOUVENIR_CREATION = {
    "lat": float,
    "lng": float,
    "scarf_id": int,
    "display_name": str,
    "age": int,
    "movement": str,
    "souvenir_type": str,
    "souvenir_content": str,
    "word_list": str,
}

SOUVENIR_TYPES = ["url", "text", "none"]


@souvenir_bp.route("/souvenirs", methods=["GET"])
@auth_token_required
def get_all_safe_souvenirs():
    """Get all safe souvenirs
    @@@

    #### Return JSON
    ```json
    {
        "success": true,
        "code": 200,
        "souvenirs": [
            {
                "id": 1,
                "geohash": "u09meq1b32np",
                "datetime_added": "Thu, 02 Apr 2020 10:57:29 GMT",
                "scarf_id": 0,
                "display_name": "Erwin",
                "movement": "autre",
                "age": 12,
                "souvenir_type": "url",
                "souvenir_content": "https://www.youtube.com/watch?v=knfrxj0T5NY",
                "word_list": [
                    "complicit\u00e9",
                    "bonheur",
                    "f\u00eate"
                ],
                "is_safe": true
            },
            ...
        ]
    }
    ```
    @@@
    """
    souvenir_list = []
    for souvenir in Souvenir.select().where(Souvenir.is_safe == True):  # noqa
        souvenir_list.append(souvenir.get_info())
    return SuccessOutput("souvenirs", souvenir_list)


@souvenir_bp.route("/souvenirs", methods=["POST"])
@auth_token_required
@validate_required_params(REQUIRED_ARGS_SOUVENIR_CREATION)
def create_souvenir():
    """Create a new souvenir
    @@@
    ### JSON args
    | args | nullable | type | remark |
    |--------|--------|--------|--------|
    |    lat    |    false    |    float   |    Latitude coord of souvenir    |
    |    lng    |    false    |    float   |    Longitude coord of souvenir    |
    |    scarf_id    |    false    |    int   |    Scarf id used as pin on the map    |
    |    display_name    |    false    |    str   |    Display name of user who created souvenir    |
    |    age    |    false    |    int   |    Age of user    |
    |    movement    |    false    |    str   |    Movement of user.    |
    |    souvenir_type    |    false    |    str   |    Souvenir type. One of `["url", "text", "none"]`    |
    |    souvenir_content    |    false    |    str   |    Content of souvenir    |
    |    word_list    |    false    |    str-json   |    List of 3 words formatted as    |
    ### JSON success

    ```json
    {
        "success": true,
        "code": 200,
        "souvenir": {
            "id": 1,
            "geohash": "u09meq1b32np",
            "datetime_added": "Thu, 02 Apr 2020 10:57:29 GMT",
            "scarf_id": 0,
            "display_name": "Erwin",
            "movement": "autre",
            "age": 12,
            "souvenir_type": "url",
            "souvenir_content": "https://www.youtube.com/watch?v=knfrxj0T5NY",
            "word_list": [
                "complicit\u00e9",
                "bonheur",
                "f\u00eate"
            ],
            "is_safe": true
        }
    }
    ```
    @@@
    """
    data = request.get_json()
    lat = data["lat"]
    lng = data["lng"]
    scarf_id = data["scarf_id"]
    display_name = data["display_name"]
    age = data["age"]
    movement = data["movement"]
    souvenir_type = data["souvenir_type"]
    souvenir_content = data["souvenir_content"]
    word_list = data["word_list"]

    error_list = []

    # convert lat long to geohash
    geohash = gh_encode(lat, lng)
    # Check scarf_id
    try:
        Scarf.get_by_id(scarf_id)
    except Scarf.DoesNotExist:
        error_list.append(
            {"field": "scarf_id", "message": "Scarf {} not found".format(scarf_id)}
        )
    # Check 0 < age > 120
    if age <= 0 or age > 120:
        error_list.append(
            {
                "field": "age",
                "message": "Incorrect age {}. Must be between 0 and 120".format(age),
            }
        )
    # Check movement list
    if movement not in MOVEMENT_LIST:
        error_list.append(
            {
                "field": "movement",
                "message": "Incorrect movement {}. Must be one of {}".format(
                    movement, MOVEMENT_LIST
                ),
            }
        )
    # Check souvenir type
    if souvenir_type not in SOUVENIR_TYPES:
        error_list.append(
            {
                "field": "souvenir_type",
                "message": "Incorrect souvenir type {}. Must be one of {}".format(
                    souvenir_type, SOUVENIR_TYPES
                ),
            }
        )
    if souvenir_type == "none":
        souvenir_content = "/"
    # check word list
    try:
        word_list = json.loads(word_list)["words"]
    except json.decoder.JSONDecodeError:
        error_list.append(
            {"field": "word_list", "message": "JSON word list malformed."}
        )
    except KeyError:
        error_list.append(
            {"field": "word_list", "message": "key of word list is malformed."}
        )

    for word in word_list:
        if " " in word:
            error_list.append({"field": "word_list", "message": "Space found in word"})

    if error_list:
        return (
            jsonify({"success": False, "status_code": 400, "errors": error_list}),
            400,
        )
    # clean word list if no error
    if not error_list:
        # clean word
        word_list = (clean(word) for word in word_list if word)
        # remove empty word generated after cleaning : "" (see clean method)
        word_list = [word for word in word_list if word]

    try:
        new_souvenir = Souvenir(
            geohash=geohash,
            scarf_id=scarf_id,
            display_name=display_name,
            age=age,
            movement=movement,
            souvenir_type=souvenir_type,
            souvenir_content=souvenir_content,
            word_list=word_list,
        )
        new_souvenir.save()
    except IntegrityError as e:
        if "souvenir_scarf_id" in str(e):
            error_list.append(
                {
                    "field": "scarf_id",
                    "message": "Scarf id {} is already used".format(scarf_id),
                }
            )
        elif "souvenir_display_name" in str(e):
            error_list.append(
                {
                    "field": "display_name",
                    "message": "Display name {} is already used".format(display_name),
                }
            )
        else:
            raise e
    if error_list:
        return (
            jsonify({"success": False, "status_code": 400, "errors": error_list}),
            400,
        )
    else:
        return SuccessOutput("souvenir", new_souvenir.get_info())


@souvenir_bp.route("/souvenirs/wordcloud", methods=["GET"])
@auth_token_required
def get_word_cloud():
    """Get word cloud
    @@@

    #### Return JSON
    ```json
    {
        "success": true,
        "code": 200,
        "word_cloud": [
            {
                "word": "fête",
                "weight": 50
            },
            {
                "word": "bonheur",
                "weight": 29
            }
            ...
        ]
    }
    ```
    @@@
    """
    words = []
    for souvenir in Souvenir.select():
        if souvenir.is_safe:
            words = words + souvenir.word_list

    return SuccessOutput("word_cloud", get_words_by_weight(words))
