# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import datetime

import flask_login as login
from flask_admin.contrib.peewee import ModelView
from peewee import AutoField
from peewee import CharField
from peewee import DateTimeField
from peewee import Model
from playhouse.postgres_ext import JSONField

from exploreve import explodb


class TrombiEntry(Model):
    """
    Model for entries in the trombinoscope
    """

    class Meta:
        database = explodb  # type: ignore

    # User's ID (primary key)
    id = AutoField()
    # The displayed name
    display_name = CharField(null=False)
    # The user's creation date
    creation_date = DateTimeField(default=datetime.datetime.utcnow)
    # When the user was last modified (by him or an admin)
    last_modified = DateTimeField(default=datetime.datetime.utcnow)
    # Profile picture link
    image_url = CharField(default="https://cdn.exploreve.fr/content/carte/fantome.svg")
    # Mouvement
    movement = CharField()
    # Roles
    roles = JSONField()

    def get_id(self) -> int:
        return int(self.id)

    # Required for administrative interface
    def __unicode__(self):
        return self.display_name

    def get_info(self) -> dict:
        user_dict = {
            "id": self.id,
            "display_name": self.display_name,
            "creation_date": self.creation_date,
            "last_modified": self.last_modified,
            "image_url": self.image_url,
            "movement": self.movement,
            "roles": self.roles,
        }
        return user_dict


class TrombiEntryAdmin(ModelView):
    model_class = TrombiEntry

    def is_accessible(self):
        return login.current_user.is_authenticated


if not TrombiEntry.table_exists():
    TrombiEntry.create_table()
