# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint
from peewee import DoesNotExist

from exploreve.errors import NotFoundError
from exploreve.models.hint import Hint
from exploreve.success.success import SuccessOutput
from exploreve.utils.decorators import auth_token_required

hint_bp = Blueprint("hints", __name__)


@hint_bp.route("/hints/<int:immersion_id>/<int:level_id>", methods=["GET"])
@auth_token_required
def get_level_hints(immersion_id, level_id):
    """Get hints for a specific level
    @@@
    #### URL args
    | args | nullable | type | remark |
    |--------|--------|--------|--------|
    |    immersion_id    |    false    |    int   |    Immersion id    |
    |    level_id    |    false    |    int   |    Level id    |

    #### return JSON
    - ##### On success:

    ```json
    {
        "success": true,
        "code": 200,
        "hints": [
            {
                "id": 1,
                "immersion_id": 1,
                "level_id": 1,
                "is_ready": true,
                "content": "lorem ipsum"
            },
            ...
        ]
    }
    ```

    - ##### On error:
    ```json
    {
        "success": false
        "code": 404,
        "error": {
            "message": "Hints for {}/{} not found",
            "name": "Not Found Error",
            "solution": "Try again",
            "type": "NotFoundError"
        }
    }
    ```
    @@@
    """
    try:
        hints = Hint.select().where(
            Hint.immersion_id == immersion_id,
            Hint.level_id == level_id,
            Hint.is_ready == True,  # noqa
        )
    except DoesNotExist:
        raise NotFoundError(
            "Hints for {}/{} not found".format(immersion_id, level_id), "Try again"
        )
    ret = []
    for h in hints:
        ret.append(h.get_info())
    return SuccessOutput("hints", ret)
