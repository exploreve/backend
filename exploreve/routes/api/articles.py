# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint
from peewee import DoesNotExist

from exploreve.errors import NotFoundError
from exploreve.models.article import Article
from exploreve.success.success import SuccessOutput
from exploreve.utils.decorators import auth_token_required

article_bp = Blueprint("articles", __name__)


@article_bp.route("/articles", methods=["GET"])
@auth_token_required
def get_all_published_articles():
    """Get all published articles
    @@@
    #### return JSON
    ```json
    {
        "success": true,
        "code": 200,
        "articles": [
            {
                "id": 15,
                "title": "Bienvenue"
                "slug": "bienvenue-grT8SRG6",
                "main_image": "https://cdn.exploreve.fr/content/articles/article1.jpg",
                "main_image_alt_text": "Rejoins l'ExploR\u00eave",
                "content": "Nam eget, vivamus nec.",
                "datetime_published": "Fri, 27 Mar 2020 20:00:00 GMT",
                "is_published": true
            },
            ...
        ]
    }
    ```
    @@@
    """
    article_list = []
    for article in Article.select():
        if article.is_published:
            article_list.append(article.get_info())
    return SuccessOutput("articles", article_list)


@article_bp.route("/articles/unpublished", methods=["GET"])
@auth_token_required
def get_all_unpublised_articles():
    """Get all unpublished articles
    @@@
    #### return JSON
    ```json
    {
        "success": true,
        "code": 200,
        "articles": [
            {
                "id": 15,
                "title": "Bienvenue"
                "slug": "bienvenue-grT8SRG6",
                "main_image": "https://cdn.exploreve.fr/content/articles/article1.jpg",
                "main_image_alt_text": "Rejoins l'ExploR\u00eave",
                "content": "Nam eget, vivamus nec.",
                "datetime_published": "Fri, 27 Mar 2020 20:00:00 GMT",
                "is_published": false
            },
            ...
        ]
    }
    ```
    @@@
    """
    article_list = []
    for article in Article.select():
        if not article.is_published:
            article_list.append(article.get_info())
    return SuccessOutput("articles", article_list)


@article_bp.route("/articles/<int:id>", methods=["GET"])
@auth_token_required
def get_article_by_id(id):
    """Get an Article by ID
    @@@
    #### JSON args
    | args | nullable | type | remark |
    |--------|--------|--------|--------|
    |    id    |    false    |    int   |    Article id    |
    #### return JSON
    - ##### On success:
    ```json
    {
        "success": true,
        "code": 200,
        "article": {
            "id": 15,
            "title": "Bienvenue"
            "slug": "bienvenue-grT8SRG6",
            "main_image": "https://cdn.exploreve.fr/content/articles/article1.jpg",
            "main_image_alt_text": "Rejoins l'ExploR\u00eave",
            "content": "Nam eget, vivamus nec.",
            "datetime_published": "Fri, 27 Mar 2020 20:00:00 GMT",
            "is_published": false
        }
    }
    ```
    - ##### On error:
    ```json
    {
        "success": false
        "code": 404,
        "error": {
            "message": "Article {} not found",
            "name": "Not Found Error",
            "solution": "Try again",
            "type": "NotFoundError"
        }
    }
    ```
    @@@
    """
    try:
        article = Article.get_by_id(id)
    except DoesNotExist:
        raise NotFoundError("Article {} not found", "Try again")
    return SuccessOutput("article", article.get_info())
