# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Tuple

from flask import Blueprint

from exploreve.errors import NotFoundError
from exploreve.models.camp import CampTile
from exploreve.success.success import SuccessOutput
from exploreve.utils.decorators import auth_token_required

camp_bp = Blueprint("camps", __name__)


def validate_int(x: str, y: str) -> Tuple[int, int]:
    """Validate that the given x and y can be casted as ints"""
    try:
        ix, iy = int(x), int(y)
    except ValueError:
        raise NotFoundError("Invalid x,y", "Try again")
    else:
        return ix, iy


@camp_bp.route("/camp/tiles/", methods=["GET"])
@camp_bp.route("/camp/tiles", methods=["GET"])
@auth_token_required
def get_all_camptiles():
    """Get all Camp Tiles
    @@@
    - #### return JSON
    ```json
    {
        "success": true,
        "code": 200,
        "tiles": [
            {
                "id": 244,
                "x": 3,
                "y": -2,
                "is_reviewed": false,
                "is_safe": true,
                "is_submitted": true,
                "datetime_added": "Fri, 27 Mar 2020 18:50:07 GMT",
                "filepath": "camp/3/-2.png",
                "url": "/camp/tiles/3/-2/",
                "extension": "png"
            },
            ...
        ]
    }
    ```
    ### Notes:
        - `is_reviewed` = Has the file been seen by a moderator (default=`false`)
        - `is_safe` = Is the tile safe to view (default=`true`)
        - `is_submitted` = Is the tile submitted and stored in the cellar
    @@@
    """
    tile_list = []
    for tile in CampTile.select():
        tile_list.append(tile.get_info())
    return SuccessOutput("tiles", tile_list)


@camp_bp.route("/camp/tiles/safe/", methods=["GET"])
@camp_bp.route("/camp/tiles/safe", methods=["GET"])
@auth_token_required
def get_all_safe_camptiles():
    """Get all safe Camp Tiles
    @@@
    #### return JSON
    ```json
    {
        "success": true,
        "code": 200,
        "tiles": [
            {
                "id": 244,
                "x": 3,
                "y": -2,
                "is_reviewed": false,
                "is_safe": true,
                "is_submitted": true,
                "datetime_added": "Fri, 27 Mar 2020 18:50:07 GMT",
                "filepath": "camp/3/-2.png",
                "url": "/camp/tiles/3/-2/",
                "extension": "png"
            },
            ...
        ]
    }
    ```
    ### Notes:
        - `is_reviewed` = Has the file been seen by a moderator (default=`false`)
        - `is_safe` = Is the tile safe to view (always `true`)
        - `is_submitted` = Is the tile submitted and stored in the cellar
    @@@
    """
    tile_list = []
    for tile in CampTile.select().where(CampTile.is_safe == True):  # noqa: E712
        tile_list.append(tile.get_info())
    return SuccessOutput("tiles", tile_list)


@camp_bp.route("/camp/tiles/safe/url/", methods=["GET"])
@camp_bp.route("/camp/tiles/safe/url", methods=["GET"])
@auth_token_required
def get_all_safe_camptiles_with_url():
    """Get all safe Camp Tiles with URL
    @@@
    #### return JSON
    ```json
    {
        "success": true,
        "code": 200,
        "tiles": [
            {
                "id": 244,
                "x": 3,
                "y": -2,
                "is_reviewed": false,
                "is_safe": true,
                "is_submitted": true,
                "datetime_added": "Fri, 27 Mar 2020 18:50:07 GMT",
                "filepath": "camp/3/-2.png",
                "url": "/camp/tiles/3/-2/",
                "extension": "png"
                "img_url": "https://cdn.exploreve.fr/camp/3/-2.png?AWSAccessKeyId=...&Signature=...&Expires=..."
            },
            ...
        ]
    }
    ```
    ### Notes:
        - `is_reviewed` = Has the file been seen by a moderator (default=`false`)
        - `is_safe` = Is the tile safe to view (always `true`)
        - `is_submitted` = Is the tile submitted and stored in the cellar
    @@@
    """
    tile_list = []
    for tile in CampTile.select().where(CampTile.is_safe == True):  # noqa: E712
        tile_info = tile.get_info()
        tile_info["img_url"] = tile.get_url()
        tile_list.append(tile_info)
    return SuccessOutput("tiles", tile_list)


@camp_bp.route("/camp/tiles/<x>/<y>/", methods=["GET"])
@camp_bp.route("/camp/tiles/<x>/<y>", methods=["GET"])
@auth_token_required
def get_camptile(x, y):
    """Get Camp Tiles at coordinates (x,y)
    @@@
    #### return JSON
    ```json
    {
        "success": true,
        "code": 200,
        "tile": {
            "id": 244,
            "x": 3,
            "y": -2,
            "is_reviewed": false,
            "is_safe": true,
            "is_submitted": true,
            "datetime_added": "Fri, 27 Mar 2020 18:50:07 GMT",
            "filepath": "camp/3/-2.png",
            "url": "/camp/tiles/3/-2/",
            "extension": "png"
            "img_url": "https://cdn.exploreve.fr/camp/3/-2.png?AWSAccessKeyId=...&Signature=...&Expires=..."
        }
    }
    ```
    ### Notes:
        - `is_reviewed` = Has the file been seen by a moderator (default=`false`)
        - `is_safe` = Is the tile safe to view (default=`true`)
        - `is_submitted` = Is the tile submitted and stored in the cellar
    @@@
    """
    x, y = validate_int(x, y)
    try:
        tile = CampTile.get(x=x, y=y)
    except CampTile.DoesNotExist:
        raise NotFoundError("Tile does not exist", "Please create it")
    tile_info = tile.get_info()
    tile_info["img_url"] = tile.get_url()
    return SuccessOutput("tile", tile_info)


@camp_bp.route("/camp/tiles/", methods=["POST"])
@camp_bp.route("/camp/tiles", methods=["POST"])
@auth_token_required
def put_camptile():
    """Create camptile
    @@@
    #### return JSON
    ```json
    {
        "code": 201,
        "success": true,
        "tile": {
            "datetime_added": "Wed, 01 Apr 2020 18:53:51 GMT",
            "extension": "png",
            "filepath": "camp/0/4.png",
            "id": 306,
            "is_reviewed": false,
            "is_safe": true,
            "is_submitted": false,
            "url": "/camp/tiles/0/4/",
            "x": 0,
            "y": 4
        }
    }
    ```
    ### Notes:
        - `is_reviewed` = Has the file been seen by a moderator (default=`false`)
        - `is_safe` = Is the tile safe to view (default=`true`)
        - `is_submitted` = Is the tile submitted and stored in the cellar
    @@@
    """
    tile = CampTile.create_next()
    tile_info = tile.get_info()
    return SuccessOutput("tile", tile_info, status_code=201)


@camp_bp.route("/camp/tiles/<x>/<y>/form/", methods=["GET"])
@camp_bp.route("/camp/tiles/<x>/<y>/form", methods=["GET"])
@auth_token_required
def get_camptile_form(x, y):
    """Get camptile form
    @@@
    #### return JSON
    ```json
    {
        "code": 200,
        "success": true,
        "tile": {
            "datetime_added": "Wed, 01 Apr 2020 18:53:51 GMT",
            "extension": "png",
            "filepath": "camp/0/4.png",
            "form": {
                "fields": {
                    "AWSAccessKeyId": "key ID",
                    "key": "camp/0/4.png",
                    "policy": "base64encoded",
                    "signature": "base64sig"
                },
                "url": "https://cellar-c2.services.clever-cloud.com/bucket"
            },
            "id": 306,
            "is_reviewed": false,
            "is_safe": true,
            "is_submitted": false,
            "url": "/camp/tiles/0/4/",
            "x": 0,
            "y": 4
        }
    }
    ```
    ### Notes:
        - `is_reviewed` = Has the file been seen by a moderator (default=`false`)
        - `is_safe` = Is the tile safe to view (default=`true`)
        - `is_submitted` = Is the tile submitted and stored in the cellar (default=`false`)
    @@@
    """
    x, y = validate_int(x, y)
    try:
        tile = CampTile.get(x=x, y=y)
    except CampTile.DoesNotExist:
        raise NotFoundError("Tile does not exist", "Please create it")
    tile_info = tile.get_info()
    tile_info["form"] = tile.get_form()
    return SuccessOutput("tile", tile_info)


@camp_bp.route("/camp/tiles/<x>/<y>/near", methods=["GET"])
@camp_bp.route("/camp/tiles/<x>/<y>/near/", methods=["GET"])
@auth_token_required
def get_nearby_camptiles(x, y):
    """Get nearby Camp Tiles
    @@@
    - This returns all tiles around the queried tile, maximum count is 8.
    - The queried tile is not returned.
    - Any tile may be missing if it is not yet created

    #### return JSON
    ```json
    {
        "success": true,
        "code": 200,
        "tiles": [
            {
                "id": 244,
                "x": 3,
                "y": -2,
                "is_reviewed": false,
                "is_safe": true,
                "is_submitted": true,
                "datetime_added": "Fri, 27 Mar 2020 18:50:07 GMT",
                "filepath": "camp/3/-2.png",
                "url": "/camp/tiles/3/-2/",
                "extension": "png"
                "img_url": "https://cdn.exploreve.fr/camp/3/-2.png?AWSAccessKeyId=...&Signature=...&Expires=..."
            },
            ...
        ]
    }
    ```
    ### Notes:
        - `is_reviewed` = Has the file been seen by a moderator (default=`false`)
        - `is_safe` = Is the tile safe to view (always `true`)
        - `is_submitted` = Is the tile submitted and stored in the cellar
    @@@
    """
    x, y = validate_int(x, y)
    try:
        tile = CampTile.get(x=x, y=y)
    except CampTile.DoesNotExist:
        raise NotFoundError("Tile does not exist", "Please create it")
    tiles = []
    tiles += tile.get_nearby_tiles()
    tiles_info = []
    for tile in tiles:
        tile_info = tile.get_info()
        if tile_info["is_safe"]:
            tile_info["img_url"] = tile.get_url()
            tiles_info.append(tile_info)
    return SuccessOutput("tiles", tiles_info)
