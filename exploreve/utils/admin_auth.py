# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import flask_admin as admin
import flask_login as login
from flask import redirect
from flask import render_template
from flask import request
from flask import url_for
from flask_admin import expose
from flask_admin import helpers
from werkzeug.security import generate_password_hash

from exploreve.forms.article import ArticleForm
from exploreve.forms.auth import LoginForm
from exploreve.forms.auth import RegistrationForm
from exploreve.models.article import Article
from exploreve.models.user import User
from exploreve.models.user import user_create


# Create customized index view class that handles login & registration
class MyAdminIndexView(admin.AdminIndexView):
    @expose("/")
    def index(self):
        if not login.current_user.is_authenticated:
            return redirect(url_for(".login_view"))
        return super(MyAdminIndexView, self).index()

    @expose("/login/", methods=("GET", "POST"))
    def login_view(self):
        # handle user login
        form = LoginForm(request.form)
        if helpers.validate_form_on_submit(form):
            user = User.get(User.email == form.email.data)
            if user.is_confirmed:
                login.login_user(user)

        if login.current_user.is_authenticated:
            return redirect(url_for(".index"))
        link = (
            "<p>Don't have an account? <a href=\""
            + url_for(".register_view")
            + '">Click here to register.</a></p>'
        )
        self._template_args["form"] = form
        self._template_args["link"] = link
        return super(MyAdminIndexView, self).index()

    @expose("/register/", methods=("GET", "POST"))
    def register_view(self):
        form = RegistrationForm(request.form)
        if helpers.validate_form_on_submit(form):
            user_create(
                first_name=form.first_name.data,
                last_name=form.last_name.data,
                email=form.email.data,
                hashed_password=generate_password_hash(form.password.data),
            )
            return redirect(url_for(".registered_view"))
        link = (
            '<p>Already have an account? <a href="'
            + url_for(".login_view")
            + '">Click here to log in.</a></p>'
        )
        self._template_args["form"] = form
        self._template_args["link"] = link
        return super(MyAdminIndexView, self).index()

    @expose("/registered/", methods=["GET"])
    def registered_view(self):
        return render_template("admin/registered.html")

    @expose("/users/confirm", methods=["GET", "POST"])
    def admin_confirm_user(self):
        if not login.current_user.is_authenticated:
            return redirect(url_for(".index"))

        email = request.form.get("email")
        if email:
            # Do an atomic update:
            (
                User.update({User.is_confirmed: True})
                .where(User.email == email)
                .execute()
            )
        user_list = []
        for user in User.select():
            if not user.is_confirmed:
                user_list.append(user.get_info())
        return render_template("admin/confirm_users.html", users=user_list)

    @expose("/articles/create", methods=["GET", "POST"])
    def admin_create_article(self):
        if not login.current_user.is_authenticated:
            return redirect(url_for(".index"))
        form = ArticleForm(request.form)
        if helpers.validate_form_on_submit(form):
            Article(
                title=form.title.data,
                content=form.content.data,
                main_image=form.main_image.data,
                main_image_alt_text=form.main_image_alt_text.data,
            ).save()
            return redirect(url_for(".index"))
        return render_template("article_creation.html", form=form)

    @expose("/logout/")
    def logout_view(self):
        login.logout_user()
        return redirect(url_for(".index"))
