# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import flask_login as login
from flask_admin.contrib.peewee import ModelView
from peewee import AutoField
from peewee import BooleanField
from peewee import IntegerField
from peewee import Model
from peewee import TextField

from exploreve import explodb


class Hint(Model):
    """
    Model for hints
    """

    class Meta:
        database = explodb  # type: ignore

    # User's ID (primary key)
    id = AutoField()
    immersion_id = IntegerField(null=False)
    level_id = IntegerField(null=False)
    is_ready = BooleanField(default=False)
    content = TextField(null=False)

    def get_id(self) -> int:
        return int(self.id)

    # Required for administrative interface
    def __unicode__(self):
        return self.display_name

    def get_info(self) -> dict:
        user_dict = {
            "id": self.id,
            "level_id": self.level_id,
            "immersion_id": self.immersion_id,
            "is_ready": self.is_ready,
            "content": self.content,
        }
        return user_dict


class HintAdmin(ModelView):
    model_class = Hint

    def is_accessible(self):
        return login.current_user.is_authenticated


if not Hint.table_exists():
    Hint.create_table()
