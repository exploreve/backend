# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from collections import Counter
from typing import Dict
from typing import List

from nltk.stem.snowball import FrenchStemmer


def most_frequent(list: List[str]) -> str:
    occurrence_count = Counter(list)
    return occurrence_count.most_common(1)[0][0]


def get_words_by_weight(words: List[str]) -> List[Dict]:
    fs = FrenchStemmer()
    stemmed_words = []
    words_by_stemmed_word: Dict[str, List[str]] = {}

    for w in words:
        stemmed_word = fs.stem(w)
        stemmed_words.append(stemmed_word)

        if stemmed_word in words_by_stemmed_word:
            words_by_stemmed_word[stemmed_word].append(w.lower())

        else:
            words_by_stemmed_word[stemmed_word] = [w.lower()]

    occurrence_count = Counter(stemmed_words)
    result = [
        {"word": most_frequent(words_by_stemmed_word[elem[0]]), "weight": elem[1]}
        for elem in occurrence_count.most_common()
    ]

    return result
