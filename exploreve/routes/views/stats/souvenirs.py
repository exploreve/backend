# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint
from flask import render_template

from exploreve.utils.color import get_random_color_sets
from exploreve.utils.stats.souvenirs import _get_souvenir_age_stats
from exploreve.utils.stats.souvenirs import _get_souvenir_content_stats
from exploreve.utils.stats.souvenirs import _get_souvenir_movement_stats
from exploreve.utils.stats.souvenirs import _get_souvenir_wordcloud_stats

stats_view_souvenir_bp = Blueprint("stats_view_souvenirs", __name__)


@stats_view_souvenir_bp.route("/stats/view/souvenirs/others", methods=["GET"])
def souvenir_other_stats():
    w = _get_souvenir_wordcloud_stats()
    total_words = w["total"]
    total_different_words = w["number_different_words"]
    labels_wc = [w["top_3"][0][0], w["top_3"][1][0], w["top_3"][2][0]]
    data_wc = [w["top_3"][0][1], w["top_3"][1][1], w["top_3"][2][1]]
    color_sets_wc = get_random_color_sets(3)
    wordcloud = {
        "total": total_words,
        "total_different": total_different_words,
        "labels": labels_wc,
        "data": data_wc,
        "color_sets": color_sets_wc,
    }

    a = _get_souvenir_age_stats()
    total_different_ages = a["num_different"]
    total_ages = a["total"]
    mean_age = a["mean"]
    median_age = a["median"]
    labels_age = [a["top_3"][0][0], a["top_3"][1][0], a["top_3"][2][0]]
    data_age = [a["top_3"][0][1], a["top_3"][1][1], a["top_3"][2][1]]
    color_sets_ages = get_random_color_sets(3)
    ages = {
        "total_different": total_different_ages,
        "total": total_ages,
        "mean": mean_age,
        "median": median_age,
        "labels": labels_age,
        "data": data_age,
        "color_sets": color_sets_ages,
    }

    m = _get_souvenir_movement_stats()
    labels_movement = [m["top_3"][0][0], m["top_3"][1][0], m["top_3"][2][0]]
    data_movement = [m["top_3"][0][1], m["top_3"][1][1], m["top_3"][2][1]]
    color_sets_movement = get_random_color_sets(3)
    movements = {
        "labels": labels_movement,
        "data": data_movement,
        "color_sets": color_sets_movement,
    }
    c = _get_souvenir_content_stats()
    color_sets_type = get_random_color_sets(3)
    types = {
        "labels": [c["types"][0][0], c["types"][1][0], c["types"][2][0]],
        "data": [c["types"][0][1], c["types"][1][1], c["types"][2][1]],
        "color_sets": color_sets_type,
        "mean": c["text"]["mean_size"],
        "median": c["text"]["median_size"],
    }

    color_sets_urls = get_random_color_sets(3)
    urls = {
        "labels": [
            c["url"]["top_3"][0][0],
            c["url"]["top_3"][1][0],
            c["url"]["top_3"][2][0],
        ],
        "data": [
            c["url"]["top_3"][0][1],
            c["url"]["top_3"][1][1],
            c["url"]["top_3"][2][1],
        ],
        "color_sets": color_sets_urls,
        "total": c["url"]["total"],
        "total_different": c["url"]["num_different"],
    }

    return render_template(
        "stats/souvenirs.html",
        wordcloud=wordcloud,
        ages=ages,
        movements=movements,
        types=types,
        urls=urls,
    )
