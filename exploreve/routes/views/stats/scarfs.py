# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint
from flask import render_template

from exploreve.utils.stats.scarfs import get_scarfs_stats

stats_view_scarfs_bp = Blueprint("stats_view_scarfs", __name__)


@stats_view_scarfs_bp.route("/stats/view/scarfs", methods=["GET"])
def view_scarfs():
    # TODO: For fun, a scarf that cycles through all colors with JS
    ret = get_scarfs_stats()
    return render_template(
        "stats/scarfs.html",
        most_of_background=ret["most_of"]["background"],
        most_of_border=ret["most_of"]["border"],
        most_of_knot=ret["most_of"]["knot"],
        median_background=ret["median"]["background"],
        median_border=ret["median"]["border"],
        median_knot=ret["median"]["knot"],
        mean_background=ret["mean"]["background"],
        mean_border=ret["mean"]["border"],
        mean_knot=ret["mean"]["knot"],
    )
