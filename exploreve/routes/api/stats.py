# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint

from exploreve.models.progress_tracker import ProgressTracker
from exploreve.success.success import SuccessOutput
from exploreve.utils.stats._per_day import get_stats_per_day
from exploreve.utils.stats.hints import get_hints_stats
from exploreve.utils.stats.immersions import get_immersion_stats
from exploreve.utils.stats.messages import get_messages_stats
from exploreve.utils.stats.newsletter import get_newsletter_stats
from exploreve.utils.stats.scarfs import get_scarfs_stats
from exploreve.utils.stats.souvenirs import get_souvenirs_stats
from exploreve.utils.stats.tiles import get_tiles_stats

stats_bp = Blueprint("stats", __name__)

# TODO: souvenir stats
#  Find where most souvenirs are at


@stats_bp.route("/stats/", methods=["GET"])
def get_all_stats():
    days, mean_per_day, median_per_day = get_stats_per_day(
        table_name="progresstracker", dt_row_name="date_started"
    )
    response = {
        "total_player_count": ProgressTracker.select().count(),
        "all_players": {
            "days": days,
            "mean_per_day": mean_per_day,
            "median_per_day": median_per_day,
        },
        "hints": get_hints_stats(),
        "newsletter": get_newsletter_stats(),
        "scarfs": get_scarfs_stats(),
        "tiles": get_tiles_stats(),
        "souvenirs": get_souvenirs_stats(),
        "messages": get_messages_stats(),
        "immersions": [
            get_immersion_stats(immersion_id=1, end_level=9, approx_end_level=8),
            get_immersion_stats(immersion_id=2, end_level=7, approx_end_level=5),
        ],
    }
    return SuccessOutput("stats", response)
