# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint
from flask import render_template

from exploreve.utils.color import get_random_color_sets
from exploreve.utils.stats.tiles import get_tiles_stats

stats_view_tiles_bp = Blueprint("stats_view_tiles", __name__)


@stats_view_tiles_bp.route("/stats/view/tiles", methods=["GET"])
def view_tiles():
    ret = get_tiles_stats()
    labels_mod = ["Unmoderated", "Moderated"]
    labels_safe = ["Unsafe", "Safe"]
    data_mod = [ret["unmoderated_count"], ret["moderated_count"]]
    data_safe = [ret["unsafe_count"], ret["safe_count"]]

    color_set1 = get_random_color_sets(2)
    color_set2 = get_random_color_sets(2)
    return render_template(
        "stats/tiles.html",
        labels_mod=labels_mod,
        data_mod=data_mod,
        labels_safe=labels_safe,
        data_safe=data_safe,
        color_set1=color_set1,
        color_set2=color_set2,
    )
