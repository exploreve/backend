# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import datetime
from typing import Any
from typing import Dict

import flask_login as login
from flask_admin.contrib.peewee import ModelView
from peewee import AutoField
from peewee import CharField
from peewee import DateTimeField
from peewee import Model

from exploreve import explodb


class Scarf(Model):
    """A scarf"""

    id = AutoField()
    border_color = CharField(max_length=7)
    background_color = CharField(max_length=7)
    knot_color = CharField(max_length=7)
    datetime_added = DateTimeField(default=datetime.datetime.utcnow)

    class Meta:
        database = explodb  # type: ignore

    def get_info(self) -> Dict[str, Any]:
        return {
            "id": self.id,
            "border_color": self.border_color,
            "background_color": self.background_color,
            "knot_color": self.knot_color,
            "url": "/scarfs/" + str(self.id),
            "datetime_added": self.datetime_added,
        }


class ScarfAdmin(ModelView):
    model_class = Scarf

    def is_accessible(self):
        return login.current_user.is_authenticated


if not Scarf.table_exists():
    Scarf.create_table()
