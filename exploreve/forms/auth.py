# Copyright (C) 2020 ExploRêve
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from peewee import DoesNotExist
from werkzeug.security import check_password_hash
from wtforms import fields
from wtforms import form
from wtforms import validators

from exploreve.models.user import User


def validate_email(form, field):
    try:
        u = User.get(User.email == field.data)
    except DoesNotExist:
        raise validators.ValidationError("Unknown email")
    if not u.is_confirmed:
        raise validators.ValidationError("You need to be confirmed by an admin")


def validate_password(form, field):
    try:
        user = User.get(User.email == field.data)
    except DoesNotExist:
        pass
    else:
        if not check_password_hash(user.password, form.password.data):
            raise validators.ValidationError("Invalid password")


# Define login and registration forms (for flask-login)
class LoginForm(form.Form):
    email = fields.StringField(validators=[validators.required(), validate_email])
    password = fields.PasswordField(
        validators=[validators.required(), validate_password]
    )


def validate_email_taken(form, field):
    try:
        User.get(User.email == field.data)
    except DoesNotExist:
        pass
    else:
        raise validators.ValidationError("Email taken")


class RegistrationForm(form.Form):
    first_name = fields.StringField(validators=[validators.required()])
    last_name = fields.StringField(validators=[validators.required()])
    email = fields.StringField(
        validators=[
            validators.email(),
            validators.input_required(),
            validate_email_taken,
        ]
    )
    password = fields.PasswordField(validators=[validators.required()])
